-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 31 Agu 2018 pada 13.24
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `website_sisfodapebar`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `kd_barang` varchar(20) NOT NULL,
  `jenis_barang` varchar(20) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `satuan` varchar(20) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `biaya_simpan` int(11) NOT NULL,
  `lead_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `kd_barang_masuk` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `kd_pengadaan` varchar(20) NOT NULL,
  `id_suplier` varchar(20) NOT NULL,
  `kd_barang` varchar(20) NOT NULL,
  `biaya_simpan` int(11) NOT NULL,
  `harga_unit` int(11) NOT NULL,
  `biaya_pemesanan` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `biaya_penyimpanan` int(11) NOT NULL,
  `totalbiaya` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_masuk_pengaman`
--

CREATE TABLE `barang_masuk_pengaman` (
  `kd_barang_masuk_pengaman` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `kd_pengadaan` varchar(20) NOT NULL,
  `id_suplier` varchar(20) NOT NULL,
  `kd_barang` varchar(20) NOT NULL,
  `biaya_simpan` int(11) NOT NULL,
  `harga_unit` int(11) NOT NULL,
  `biaya_pemesanan` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `biaya_penyimpanan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengadaan_barang`
--

CREATE TABLE `pengadaan_barang` (
  `kd_pengadaan` varchar(20) NOT NULL,
  `tgl_pengadaan` date NOT NULL,
  `id_suplier` varchar(20) NOT NULL,
  `kd_barang` varchar(20) NOT NULL,
  `biaya_simpan` int(11) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `stok_permintaan` int(11) NOT NULL,
  `biaya_pemesanan` int(11) NOT NULL,
  `total_biaya` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `persediaan_barang`
--

CREATE TABLE `persediaan_barang` (
  `id_persediaan` int(11) NOT NULL,
  `kd_barang` varchar(20) NOT NULL,
  `stok_barang` int(11) NOT NULL,
  `stok_pengaman` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `suplier`
--

CREATE TABLE `suplier` (
  `id_suplier` varchar(20) NOT NULL,
  `nama_suplier` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `cp` varchar(50) NOT NULL,
  `no_hp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_eoq`
--

CREATE TABLE `tabel_eoq` (
  `id_eoq` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `kd_barang` varchar(20) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `biaya_pemesanan` int(11) NOT NULL,
  `biaya_penyimpanan` int(11) NOT NULL,
  `lead_time` int(11) NOT NULL,
  `permintaan` int(11) NOT NULL,
  `eoq` int(11) NOT NULL,
  `rop` int(11) NOT NULL,
  `total_biaya` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `kd_transaksi` varchar(20) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `kd_barang` varchar(20) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` varchar(20) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `level` enum('penjual','gudang','admin') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `password`, `level`) VALUES
('admin', 'Admin', '21232f297a57a5a743894a0e4a801fc3', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kd_barang`);

--
-- Indeks untuk tabel `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`kd_barang_masuk`),
  ADD KEY `id_suplier` (`id_suplier`),
  ADD KEY `kd_barang` (`kd_barang`),
  ADD KEY `kd_pengadaan` (`kd_pengadaan`);

--
-- Indeks untuk tabel `barang_masuk_pengaman`
--
ALTER TABLE `barang_masuk_pengaman`
  ADD PRIMARY KEY (`kd_barang_masuk_pengaman`),
  ADD KEY `id_suplier` (`id_suplier`),
  ADD KEY `kd_barang` (`kd_barang`),
  ADD KEY `kd_pengadaan` (`kd_pengadaan`);

--
-- Indeks untuk tabel `pengadaan_barang`
--
ALTER TABLE `pengadaan_barang`
  ADD PRIMARY KEY (`kd_pengadaan`),
  ADD KEY `id_suplier` (`id_suplier`),
  ADD KEY `kd_barang` (`kd_barang`);

--
-- Indeks untuk tabel `persediaan_barang`
--
ALTER TABLE `persediaan_barang`
  ADD PRIMARY KEY (`id_persediaan`),
  ADD KEY `kd_barang` (`kd_barang`);

--
-- Indeks untuk tabel `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id_suplier`);

--
-- Indeks untuk tabel `tabel_eoq`
--
ALTER TABLE `tabel_eoq`
  ADD PRIMARY KEY (`id_eoq`),
  ADD KEY `kd_barang` (`kd_barang`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`kd_transaksi`),
  ADD KEY `kd_barang` (`kd_barang`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `persediaan_barang`
--
ALTER TABLE `persediaan_barang`
  MODIFY `id_persediaan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tabel_eoq`
--
ALTER TABLE `tabel_eoq`
  MODIFY `id_eoq` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD CONSTRAINT `barang_masuk_ibfk_1` FOREIGN KEY (`id_suplier`) REFERENCES `suplier` (`id_suplier`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `barang_masuk_ibfk_2` FOREIGN KEY (`kd_barang`) REFERENCES `barang` (`kd_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pengadaan_barang`
--
ALTER TABLE `pengadaan_barang`
  ADD CONSTRAINT `pengadaan_barang_ibfk_1` FOREIGN KEY (`id_suplier`) REFERENCES `suplier` (`id_suplier`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengadaan_barang_ibfk_2` FOREIGN KEY (`kd_barang`) REFERENCES `barang` (`kd_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `persediaan_barang`
--
ALTER TABLE `persediaan_barang`
  ADD CONSTRAINT `persediaan_barang_ibfk_1` FOREIGN KEY (`kd_barang`) REFERENCES `barang` (`kd_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tabel_eoq`
--
ALTER TABLE `tabel_eoq`
  ADD CONSTRAINT `tabel_eoq_ibfk_1` FOREIGN KEY (`kd_barang`) REFERENCES `barang` (`kd_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`kd_barang`) REFERENCES `barang` (`kd_barang`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
