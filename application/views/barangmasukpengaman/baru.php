<h2 class="title1">Barang Masuk</h2>
<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
	<div class="form-title">
		<h4>Barang Masuk :</h4>
	</div>
	<div class="form-body">
		<?= validation_errors()?>
		<?= form_open() ?>
			<div class="form-group">
				<label for="kd_barang_masuk_pengaman">Kode Barang Masuk</label> <span onclick="kodebarangmasukpengaman()" class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
				<input type="text" class="form-control" id="kd_barang_masuk_pengaman" name="kd_barang_masuk_pengaman" placeholder="Kode Barang Masuk" required readonly="">
			</div>
			<div class="form-group">
				<label for="tanggal">Tanggal</label>
				<input type="date" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal" required>
			</div>
			<div class="form-group">
				<label for="kd_pengadaan">Kode Pengadaan</label>
				<select class="form-control" id="kd_pengadaan" name="kd_pengadaan" onclick="datapengadaan()">
					<?PHP foreach ($pengadaan as $data) {
					?>
					<option value="<?= $data->kd_pengadaan ?>"><?= $data->kd_pengadaan ?></option>
					<?PHP
					} ?>
				</select>
			</div>
			<div class="form-group">
				<label for="suplier">Suplier</label>
				<input type="text" class="form-control" id="suplier" name="suplier" placeholder="Suplier" required readonly="">
				<input type="hidden" name="id_suplier" id="id_suplier" value="">
			</div>
			<div class="form-group">
				<label for="barang">Barang</label>
				<input type="text" class="form-control" id="barang" name="barang" placeholder="Barang" required readonly="">
				<input type="hidden" name="kd_barang" id="kd_barang" value="">
			</div>
			<div class="form-group">
				<label for="biaya_simpan">% Biaya Simpan</label>
				<input type="number" class="form-control" id="biaya_simpan" name="biaya_simpan" placeholder="% Biaya Simpan" required readonly="">
			</div>
			<div class="form-group">
				<label for="harga_unit">Harga Unit</label>
				<input type="number" class="form-control" id="harga_unit" name="harga_unit" placeholder="Harga Unit" required readonly="">
			</div>
			<div class="form-group">
				<label for="biaya_pemesanan">Biaya Pemesanan</label>
				<input type="number" class="form-control" id="biaya_pemesanan" name="biaya_pemesanan" placeholder="Biaya Pemesanan" required readonly="">
			</div>
			<div class="form-group">
				<label for="stok">Stok</label>
				<input type="number" class="form-control" id="stok" name="stok" placeholder="Stok" required>
			</div>
			<div class="form-group">
				<label for="biaya_penyimpanan">Biaya Penyimpanan</label>
				<input type="number" class="form-control" id="biaya_penyimpanan" name="biaya_penyimpanan" placeholder="Biaya Penyimpanan" required readonly="">
			</div>
			<input type="submit" class="btn btn-primary" value="Simpan">
		</form> 
	</div>
</div>
<script type="text/javascript">
	
	function datapengadaan() {
		var id = document.getElementById("kd_pengadaan").value;

		var id_suplier = [];
		<?php foreach ($pengadaan as $data): ?>
			id_suplier["<?= $data->kd_pengadaan ?>"] = "<?= $data->id_suplier ?>";
		<?php endforeach ?>

		var kd_barang = [];
		<?php foreach ($pengadaan as $data): ?>
			kd_barang["<?= $data->kd_pengadaan ?>"] = "<?= $data->kd_barang ?>";
		<?php endforeach ?>

		var biaya_simpan = [];
		<?php foreach ($pengadaan as $data): ?>
			biaya_simpan["<?= $data->kd_pengadaan ?>"] = "<?= $data->biaya_simpan ?>";
		<?php endforeach ?>

		var harga_beli = [];
		<?php foreach ($pengadaan as $data): ?>
			harga_beli["<?= $data->kd_pengadaan ?>"] = "<?= $data->harga_beli ?>";
		<?php endforeach ?>

		var biaya_pemesanan = [];
		<?php foreach ($pengadaan as $data): ?>
			biaya_pemesanan["<?= $data->kd_pengadaan ?>"] = "<?= $data->biaya_pemesanan ?>";
		<?php endforeach ?>

		var stok_permintaan = [];
		<?php foreach ($pengadaan as $data): ?>
			stok_permintaan["<?= $data->kd_pengadaan ?>"] = "<?= $data->stok_permintaan ?>";
		<?php endforeach ?>

		var biaya_penyimpanan = Math.round(parseFloat(biaya_simpan[id] / 100 ) * parseFloat(harga_beli[id]));

		document.getElementById("id_suplier").value = id_suplier[id];
		document.getElementById("suplier").value = suplier(id_suplier[id]);
		document.getElementById("kd_barang").value = kd_barang[id];
		document.getElementById("barang").value = fbarang(kd_barang[id]);
		document.getElementById("biaya_simpan").value = biaya_simpan[id];
		document.getElementById("harga_unit").value = harga_beli[id];
		document.getElementById("biaya_pemesanan").value = biaya_pemesanan[id];
		document.getElementById("stok").value = stok_permintaan[id];
		document.getElementById("biaya_penyimpanan").value = biaya_penyimpanan;
	}

	function suplier(sup) {

		var suplier = [];
		<?php foreach ($suplier as $data): ?>
			suplier["<?= $data->id_suplier ?>"] = "<?= $data->nama_suplier ?>";
		<?php endforeach ?>

		return suplier[sup];
	}

	function fbarang(bar) {

		var barang = [];
		<?php foreach ($barang as $bar): ?>
			barang["<?= $bar->kd_barang ?>"] = "<?= $bar->nama_barang ?>";
		<?php endforeach ?>

		return barang[bar];
	}

	function kodebarangmasukpengaman(){
		var j = "bamp<?= $bamp ?>";
		
		document.getElementById("kd_barang_masuk_pengaman").value = j;
	}
</script>