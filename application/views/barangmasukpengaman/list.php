<h2 class="title1">Barang Masuk</h2>
<div class="grids widget-shadow">
	<table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
		<thead>
			<tr>
				<th>Kode Barang Masuk Stok Pengaman</th>
				<th>Tanggal</th>
				<th>Kode Pengadaan</th>
				<th>Suplier</th>
				<th>Barang</th>
				<th>% Biaya Simpan</th>
				<th>Harga Unit</th>
				<th>Biaya Pemesanan</th>
				<th>Stok</th>
				<th>Biaya Penyimpanan</th>
			</tr>
		</thead>
		<tbody>
<?PHP
	foreach ($barangmasukpengaman as $data) {
?>
			<tr>
				<td><?= $data->kd_barang_masuk_pengaman ?></td>
				<td><?= $data->tanggal ?></td>
				<td><?= $data->kd_pengadaan ?></td>
				<td><?= $data->id_suplier." - ".namasuplier($data->id_suplier) ?></td>
				<td><?= $data->kd_barang." - ".namabarang($data->kd_barang) ?></td>
				<td><?= $data->biaya_simpan ?>&nbsp;%</td>
				<td><?= rupiah($data->harga_unit) ?></td>
				<td><?= rupiah($data->biaya_pemesanan) ?></td>
				<td><?= $data->stok ?></td>
				<td><?= rupiah($data->biaya_penyimpanan) ?></td>
			</tr>
<?PHP
	}
?>
		</tbody>
	</table>

<script type="text/javascript">
	$(document).ready(function() {
    $('#tabelku').DataTable();
} );
</script>
</div>