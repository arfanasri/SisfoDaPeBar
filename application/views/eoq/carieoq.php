<h2 class="title1">EOQ</h2>
<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
	<div class="form-title">
		<h4>EOQ Baru :</h4>
	</div>
	<div class="form-body">
		<?= validation_errors()?>
		<?= form_open() ?>
			<div class="form-group">
				<label for="bulan">Bulan</label>
				<select class="form-control" id="bulan" name="bulan">
					<option value="01">Januari</option>
					<option value="02">Februari</option>
					<option value="03">Maret</option>
					<option value="04">April</option>
					<option value="05">Mei</option>
					<option value="06">Juni</option>
					<option value="07">Juli</option>
					<option value="08">Agustus</option>
					<option value="09">September</option>
					<option value="10">Oktober</option>
					<option value="11">November</option>
					<option value="12">Desember</option>
				</select>
			</div>
			<div class="form-group">
				<label for="tahun">Tahun</label>
				<input type="number" min="1900" max="2100" class="form-control" id="tahun" name="tahun" value="<?= date("Y") ?>">
			</div>
			<div class="form-group">
				<label for="kd_barang">Barang</label>
				<select class="form-control" id="kd_barang" name="kd_barang">
					<?php foreach ($barang as $data): ?>
						<option value="<?= $data->kd_barang ?>"><?= $data->kd_barang. " - " . $data->nama_barang?></option>
					<?php endforeach ?>
				</select>
			</div>
			<input type="submit" class="btn btn-primary" value="Proses EOQ">
		</form> 
	</div>
</div>