<?PHP
	if($print == false) {
?>
<div class="w3-white grids">
	<?= form_open() ?>
		<select name="bulan" id="bulan" onchange="cari()">
			<option <?PHP if($bulan == "01") echo "selected" ?> value="01">Januari</option>
			<option <?PHP if($bulan == "02") echo "selected" ?> value="02">Februari</option>
			<option <?PHP if($bulan == "03") echo "selected" ?> value="03">Maret</option>
			<option <?PHP if($bulan == "04") echo "selected" ?> value="04">April</option>
			<option <?PHP if($bulan == "05") echo "selected" ?> value="05">Mei</option>
			<option <?PHP if($bulan == "06") echo "selected" ?> value="06">Juni</option>
			<option <?PHP if($bulan == "07") echo "selected" ?> value="07">Juli</option>
			<option <?PHP if($bulan == "08") echo "selected" ?> value="08">Agustus</option>
			<option <?PHP if($bulan == "09") echo "selected" ?> value="09">September</option>
			<option <?PHP if($bulan == "10") echo "selected" ?> value="10">Oktober</option>
			<option <?PHP if($bulan == "11") echo "selected" ?> value="11">November</option>
			<option <?PHP if($bulan == "12") echo "selected" ?> value="12">Desember</option>
		</select>
		<input type="number" min="2000" max="3000" value="<?= $tahun ?>" name="tahun" id="tahun" onchange="cari()">
		<a href="<?= site_url("laporan/dataeoq/".$tahun."/".$bulan) ?>" id="carilink" class="btn btn-primary">Cari</a>
	</form>
</div>
<script type="text/javascript">
	function cari(){
		var a = document.getElementById("bulan").value;
		var b = document.getElementById("tahun").value;

		var ca = "<?= site_url("laporan/dataeoq/") ?>" + b + "/" + a;
		document.getElementById("carilink").href = ca;
	}
</script>
<?PHP
	}
?>
<div class="w3-white grids" id="yangdiprint">
	<table>
		<tr>
			<td style="width: 10%"><img src="<?= base_url() ?>assets/gambar/logo.png" style="width: 100%"></td>
			<td>
<h1 class="text-center w3-text-black">CAHAYA LATOLING</h1>
<h5 class="text-center w3-text-black">Toko Alat Pertanian, Alat Rumah Tangga dan Material Alat Bilah Pedang</h5>
<h6 class="text-center w3-text-black">Jl. Pemukiman No. 218 RT 1/RW 2 Lingkungan Kelurahan Massepe, Kecamatan Tellu Limpoe Kabupaten Sidenreng Rappang</h6>
			</td>
			<td style="width: 10%"></td>
		</tr>
	</table>
<hr>
<h2 class="title1 text-center w3-text-black">DATA EOQ</h2>
<div class="grids">
	<table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
		<thead>
			<tr class="w3-black">
				<th>No</th>
				<th>Tanggal</th>
				<th>Barang</th>
				<th>Harga Beli</th>
				<th>Biaya Pemesanan</th>
				<th>Biaya Penyimpanan</th>
				<th>Lead Time</th>
				<th>Permintaan</th>
				<th>EOQ</th>
				<th>ROP</th>
				<th>Total Biaya</th>
			</tr>
		</thead>
		<tbody>
<?PHP
	$no = 1;
	foreach ($eoq as $data) {
?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $data->tanggal ?></td>
				<td><?= $data->kd_barang ." - ". namabarang($data->kd_barang) ?></td>
				<td><?= rupiah($data->harga_beli) ?></td>
				<td><?= rupiah($data->biaya_pemesanan) ?></td>
				<td><?= rupiah($data->biaya_penyimpanan) ?></td>
				<td><?= $data->lead_time ?></td>
				<td><?= $data->permintaan ?></td>
				<td><?= $data->eoq ?></td>
				<td><?= $data->rop ?></td>
				<td><?= rupiah($data->total_biaya) ?></td>
			</tr>
<?PHP
	}
?>
		</tbody>
	</table>
</div>
</div>
<?PHP if ($print == true) {
?>
<script type="text/javascript">
	window.onload = function () {
    window.print();
}
</script>
<?PHP
} else {
?>
<a href="<?= site_url("laporan/dataeoq/".$tahun."/".$bulan."/print") ?>" class="btn btn-primary" >Print</a>
<?PHP
}
?>