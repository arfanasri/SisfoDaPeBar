<h2 class="title1">EOQ</h2>
<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
	<div class="form-title">
		<h4>EOQ Baru :</h4>
	</div>
	<div class="form-body">
		<?= validation_errors()?>
		<?= form_open() ?>
			<div class="form-group">
				<label for="tanggal">Tanggal</label>
				<input type="date" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal" required="" value="<?= $tanggal ?>">
			</div>
			<div class="form-group">
				<label for="kd_barang">Barang</label>
				<select class="form-control" id="kd_barang" name="kd_barang">
					<option value="<?= $barang->kd_barang ?>"><?= $barang->kd_barang. " - " . $barang->nama_barang?></option>
				</select>
			</div>
			<div class="form-group">
				<label for="harga_beli">Harga Beli</label>
				<input type="number" class="form-control" id="harga_beli" name="harga_beli" placeholder="Harga Beli" value="<?= $harga_beli ?>" required="" readonly="" >
			</div>
			<div class="form-group">
				<label for="biaya_pemesanan">Biaya Pemesanan</label>
				<input type="number" class="form-control" id="biaya_pemesanan" name="biaya_pemesanan" placeholder="Biaya Pemesanan" required="" value="<?= $biaya_pemesanan ?>" readonly="">
			</div>
			<div class="form-group">
				<label for="biaya_penyimpanan">Biaya Penyimpanan</label>
				<input type="number" class="form-control" id="biaya_penyimpanan" name="biaya_penyimpanan" placeholder="Biaya Penyimpanan" required="" readonly=""  value="<?= $biaya_penyimpanan ?>">
			</div>
			<div class="form-group">
				<label for="lead_time">Lead Time</label>
				<input type="number" class="form-control" id="lead_time" name="lead_time" placeholder="Lead Time" required="" value="<?= $barang->lead_time ?>" readonly="">
			</div>
			<div class="form-group">
				<label for="permintaan">Permintaan</label>
				<input type="number" class="form-control" id="permintaan" name="permintaan" placeholder="Permintaan" required="" value="<?= $permintaan ?>" readonly="">
			</div>
			<div class="form-group">
				<label for="eoq">EOQ</label>
				<input type="number" class="form-control" id="eoq" name="eoq" placeholder="EOQ" required="" readonly="" value="<?= $eoq ?>">
			</div>
			<div class="form-group">
				<label for="rop">ROP</label>
				<input type="number" class="form-control" id="rop" name="rop" placeholder="Biaya Pemesanan" required="" readonly="" value="<?= $rop ?>">
			</div>
			<div class="form-group">
				<label for="total_biaya">Total Biaya</label>
				<input type="number" class="form-control" id="total_biaya" name="total_biaya" placeholder="Total Biaya" required="" readonly="" value="<?= $total_biaya ?>">
			</div>
			<input type="submit" class="btn btn-primary" value="Simpan">
		</form> 
	</div>
</div>