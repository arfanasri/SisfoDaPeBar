<h2 class="title1">EOQ</h2>
<div class="grids widget-shadow">
	<table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
		<thead>
			<tr>
				<th>Tanggal</th>
				<th>Barang</th>
				<th>Harga Beli</th>
				<th>Biaya Pemesanan</th>
				<th>Biaya Penyimpanan</th>
				<th>Lead Time</th>
				<th>Permintaan</th>
				<th>EOQ</th>
				<th>ROP</th>
				<th>Total Biaya</th>
			</tr>
		</thead>
		<tbody>
<?PHP
	foreach ($eoq as $data) {
?>
			<tr>
				<td><?= $data->tanggal ?></td>
				<td><?= $data->kd_barang ." - ". namabarang($data->kd_barang) ?></td>
				<td><?= rupiah($data->harga_beli) ?></td>
				<td><?= rupiah($data->biaya_pemesanan) ?></td>
				<td><?= rupiah($data->biaya_penyimpanan) ?></td>
				<td><?= $data->lead_time ?></td>
				<td><?= $data->permintaan ?></td>
				<td><?= $data->eoq ?></td>
				<td><?= $data->rop ?></td>
				<td><?= rupiah($data->total_biaya) ?></td>
			</tr>
<?PHP
	}
?>
		</tbody>
	</table>

<script type="text/javascript">
	$(document).ready(function() {
    $('#tabelku').DataTable();
} );
</script>
</div>