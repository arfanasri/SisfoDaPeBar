<h2 class="title1">Persediaan Barang</h2>
<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
	<div class="form-title">
		<h4>Persediaan Barang Baru :</h4>
	</div>
	<div class="form-body">
		<?= validation_errors()?>
		<?= form_open() ?>
			<div class="form-group">
				<label for="bulan">Bulan</label>
				<input type="date" class="form-control" id="bulan" name="bulan" placeholder="Bulan" required="">
			</div>
			<div class="form-group">
				<label for="kd_barang">Barang</label>
				<select class="form-control" id="kd_barang" name="kd_barang">
<?PHP
	foreach ($barang as $data) {
?>
					<option value="<?= $data->kd_barang ?>"><?= $data->kd_barang ." - ". $data->nama_barang ?> </option>
<?PHP
	}
?>
				</select>
			</div>
			<div class="form-group">
				<label for="stok_barang">Stok Barang</label>
				<input type="text" class="form-control" id="stok_barang" name="stok_barang" placeholder="Stok Barang" required="">
			</div>
			<div class="form-group">
				<label for="titik_pemesanan">Titik Pemesanan</label>
				<input type="text" class="form-control" id="titik_pemesanan" name="titik_pemesanan" placeholder="Titik Pemesanan" required="">
			</div>
			<div class="form-group">
				<label for="biaya_pengelolaan">Biaya Pengelolaan</label>
				<input type="text" class="form-control" id="biaya_pengelolaan" name="biaya_pengelolaan" placeholder="Biaya Pengelolaan" required="">
			</div>
			<div class="form-group">
				<label for="biaya_pemesanan">Biaya Pemesanan</label>
				<input type="text" class="form-control" id="biaya_pemesanan" name="biaya_pemesanan" placeholder="Biaya Pemesanan" required="">
			</div>
			<input type="submit" class="btn btn-primary" value="Simpan">
		</form> 
	</div>
</div>