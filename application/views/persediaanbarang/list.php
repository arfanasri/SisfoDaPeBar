<h2 class="title1">Persediaan Barang</h2>
<div class="grids widget-shadow">
	<table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
		<thead>
			<tr>
				<th>Kode Barang</th>
				<th>Nama Barang</th>
				<th>Stok Barang</th>
			</tr>
		</thead>
		<tbody>
<?PHP
	foreach ($persediaanbarang as $data) {
?>
			<tr>
				<td><?= $data->kd_barang ?></td>
				<td><?= namabarang($data->kd_barang)?></td>
				<td><?= $data->stok_barang ?></td>
			</tr>
<?PHP
	}
?>
		</tbody>
	</table>

<script type="text/javascript">
	$(document).ready(function() {
    $('#tabelku').DataTable();
} );
</script>
</div>