<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

  // Load the Visualization API and the corechart package.
  google.charts.load('current', {'packages':['corechart']});

  // Set a callback to run when the Google Visualization API is loaded.
  google.charts.setOnLoadCallback(drawChart);

  // Callback that creates and populates a data table,
  // instantiates the pie chart, passes in the data and
  // draws it.
  function drawChart() {

    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Bulan');
    data.addColumn('number', 'Eoq');
    data.addColumn('number', 'Tanpa Eoq');
    data.addRows([
 <?php
 for($i = 1; $i < 6; $i++){
 	if($i != 1) echo ",";
 ?>
 	["<?= $bulan[$i] ?>", <?= $eoq[$i] ?>, <?= $tanpaeoq[$i] ?>]
 <?PHP
 }
 ?>
    ]);

    // Set chart options
    var options = {'title':'Grafik Perbandingan'
	};

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.ColumnChart(document.getElementById('grafik'));
    chart.draw(data, options);
  }
</script>

<h2 class="title1">Grafik</h2>
<div class="grids widget-shadow">
	<?= form_open() ?>
<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
  <div class="form-body">
	<div class="form-group">
		<label for="kd_barang">Nama BarangBarang</label>
		<select class="form-control" id="kd_barang" name="kd_barang" placeholder="Barang" required="">
			<?php foreach ($barang as $data): ?>
				<option value="<?= $data->kd_barang ?>"><?= $data->nama_barang ?></option>
			<?php endforeach ?>
		</select>
  </div>
  <br>
	<input type="submit" class="btn btn-primary" value="Lihat">
  </div>
</div>
</div>
<div class="grids widget-shadow">
	<div id="grafik"></div>
</div>