<h2 class="title1">Login</h2>
<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
	<div class="form-title">
		<h4>Login :</h4>
	</div>
	<div class="form-body">
		<?= validation_errors()?>
		<?= form_open() ?>
			<div class="form-group">
				<label for="id_user">Username</label>
				<input type="text" class="form-control" id="id_user" name="id_user" placeholder="Username" required="">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
			</div>
			<div class="form-group">
				<label for="capcha">Capcha</label> <span onclick="kodepengadaan()" class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
				<input type="capcha" class="form-control" id="capcha" name="capcha" disabled="true">
				<input type="recapcha" class="form-control" id="recapcha" name="capcha" required="" onkeyup="testcapcha()">
			</div>
			<input id="masuk" type="submit" class="btn btn-primary" value="Masuk" disabled="true">
		</form> 
	</div>
</div>
<script type="text/javascript">
	function kodepengadaan(){
		var sa = Math.floor((Math.random() * 10));
		var du = Math.floor((Math.random() * 10));
		var ti = Math.floor((Math.random() * 10));
		var em = Math.floor((Math.random() * 10));
		var li = Math.floor((Math.random() * 10));
		var en = Math.floor((Math.random() * 10));

		document.getElementById("capcha").value = sa.toString() + du.toString() + ti.toString() + em.toString() + li.toString() + en.toString();
	}

	window.onload = function () {
		kodepengadaan();
	}

	function testcapcha() {
		var a = document.getElementById("capcha").value;
		var b = document.getElementById("recapcha").value;

		if(a == b) {
			document.getElementById("masuk").disabled = false;
		} else {
			document.getElementById("masuk").disabled = true;
		}

	}
</script>