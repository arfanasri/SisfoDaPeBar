<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Sistem Informasi Data dan Pengendalian Persediaan Barang</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?= base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?= base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?= base_url(); ?>assets/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS -->

 <!-- side nav css file -->
 <link href='<?= base_url(); ?>assets/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
 <!-- side nav css file -->
 
 <!-- js-->
<script src="<?= base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- Metis Menu -->
<script src="<?= base_url(); ?>assets/js/metisMenu.min.js"></script>
<script src="<?= base_url(); ?>assets/js/custom.js"></script>
<link href="<?= base_url(); ?>assets/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

<!-- DataTables -->

<script src="<?= base_url(); ?>assets/datatables.min.js"></script>
<link href="<?= base_url(); ?>assets/datatables.min.css" rel="stylesheet">

<!--//DataTables -->

</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
		<aside class="sidebar-left">
      <nav class="navbar navbar-inverse">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <h1><a class="navbar-brand" href="<?= site_url() ?>"><span><img style="width: 40px" src="<?= base_url() ?>assets/gambar/logo.png"></span> Cahaya&nbsp;<span class="dashboard_text">Latoling</span></a></h1>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          	<ul class="sidebar-menu">
          		<li class="header">NAVIGASI</li>
          		<li class="treeview">
          			<a href="<?= site_url() ?>">
          				<i class="fa fa-dashboard"></i> <span>Beranda</span>
          			</a>
          		</li>
<!-- Super Admin -->
<?PHP if($_SESSION["pengguna"] == "admin") {
?>
          		<li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-tags"></i>
                  <span>Barang</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("barang/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("barang") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-user"></i>
                  <span>User</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("user/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("user") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-user"></i>
                  <span>Suplier</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("suplier/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("suplier") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-export"></i>
                  <span>Transaksi Penjualan</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("transaksipenjualan/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("transaksipenjualan") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-import"></i>
                  <span>Barang Masuk</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("barangmasuk/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("barangmasuk") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-import"></i>
                  <span>Barang Masuk Pengaman</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("barangmasukpengaman/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("barangmasukpengaman") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-send"></i>
                  <span>Pengadaan Barang</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("pengadaanbarang/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("pengadaanbarang") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-credit-card"></i>
                  <span>EOQ</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("eoq/cekeoq") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("eoq") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-print"></i>
                  <span>Laporan</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("laporan/databarang") ?>"><i class="fa fa-angle-right"></i> Data Barang</a></li>
                  <li><a href="<?= site_url("laporan/datasuplier") ?>"><i class="fa fa-angle-right"></i> Data Suplier</a></li>
                  <li><a href="<?= site_url("laporan/datauser") ?>"><i class="fa fa-angle-right"></i> Data User</a></li>
                  <li><a href="<?= site_url("laporan/databarangkeluar/".date("Y")."/".date("m")) ?>"><i class="fa fa-angle-right"></i> Data Barang Keluar</a></li>
                  <li><a href="<?= site_url("laporan/dataeoq/".date("Y")."/".date("m")) ?>"><i class="fa fa-angle-right"></i> Data EOQ</a></li>
                  <li><a href="<?= site_url("laporan/datapengadaanbarang/".date("Y")."/".date("m")) ?>"><i class="fa fa-angle-right"></i> Data Pengadaan Barang</a></li>
                  <li><a href="<?= site_url("laporan/databarangmasuk/".date("Y")."/".date("m")) ?>"><i class="fa fa-angle-right"></i> Data Barang Masuk</a></li>
                  <li><a href="<?= site_url("laporan/databarangmasukpengaman/".date("Y")."/".date("m")) ?>"><i class="fa fa-angle-right"></i> Data Barang Masuk Pengaman</a></li>
                  <li><a href="<?= site_url("laporan/datapersediaanbarang") ?>"><i class="fa fa-angle-right"></i> Data Persediaan Barang</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="<?= site_url("laporan/grafik") ?>">
                  <i class="fa fa-dashboard"></i> <span>Grafik</span>
                </a>
              </li>

<!-- Admin -->
<?PHP
  } elseif ($_SESSION["level"] == "admin") {
?>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-tags"></i>
                  <span>Barang</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("barang/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("barang") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-user"></i>
                  <span>User</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("user/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("user") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-user"></i>
                  <span>Suplier</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("suplier/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("suplier") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-print"></i>
                  <span>Laporan</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("laporan/databarang") ?>"><i class="fa fa-angle-right"></i> Data Barang</a></li>
                  <li><a href="<?= site_url("laporan/datasuplier") ?>"><i class="fa fa-angle-right"></i> Data Suplier</a></li>
                  <li><a href="<?= site_url("laporan/datauser") ?>"><i class="fa fa-angle-right"></i> Data User</a></li>
                  <li><a href="<?= site_url("laporan/databarangkeluar/".date("Y")."/".date("m")) ?>"><i class="fa fa-angle-right"></i> Data Barang Keluar</a></li>
                  <li><a href="<?= site_url("laporan/dataeoq/".date("Y")."/".date("m")) ?>"><i class="fa fa-angle-right"></i> Data EOQ</a></li>
                  <li><a href="<?= site_url("laporan/datapengadaanbarang/".date("Y")."/".date("m")) ?>"><i class="fa fa-angle-right"></i> Data Pengadaan Barang</a></li>
                  <li><a href="<?= site_url("laporan/databarangmasuk/".date("Y")."/".date("m")) ?>"><i class="fa fa-angle-right"></i> Data Barang Masuk</a></li>
                  <li><a href="<?= site_url("laporan/databarangmasukpengaman/".date("Y")."/".date("m")) ?>"><i class="fa fa-angle-right"></i> Data Barang Masuk Pengaman</a></li>
                  <li><a href="<?= site_url("laporan/datapersediaanbarang") ?>"><i class="fa fa-angle-right"></i> Data Persediaan Barang</a></li>
                </ul>
              </li>

<!-- Penjual -->
<?PHP
  } elseif ($_SESSION["level"] == "penjual") {
?>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-tags"></i>
                  <span>Barang</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("barang") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-user"></i>
                  <span>Suplier</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("suplier") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-export"></i>
                  <span>Transaksi Penjualan</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("transaksipenjualan/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("transaksipenjualan") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>

<!-- Gudang -->
<?PHP
  } elseif ($_SESSION["level"] == "gudang") {
?>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-tags"></i>
                  <span>Barang</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("barang") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-user"></i>
                  <span>Suplier</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("suplier") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-import"></i>
                  <span>Barang Masuk</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("barangmasuk/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("barangmasuk") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-import"></i>
                  <span>Barang Masuk Pengaman</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("barangmasukpengaman/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("barangmasukpengaman") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-send"></i>
                  <span>Pengadaan Barang</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("pengadaanbarang/tambah") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("pengadaanbarang") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="glyphicon glyphicon-credit-card"></i>
                  <span>EOQ</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?= site_url("eoq/cekeoq") ?>"><i class="fa fa-angle-right"></i> Baru</a></li>
                  <li><a href="<?= site_url("eoq") ?>"><i class="fa fa-angle-right"></i> List</a></li>
                </ul>
              </li>

<?PHP
  }
?>
          	</ul>
          </div>
          <!-- /.navbar-collapse -->
      </nav>
    </aside>
	</div>
		<!--left-fixed -navigation-->
		
		<!-- header-starts -->
		<div class="sticky-header header-section ">
			<div class="header-left">
			</div>
			<div class="header-right">				
				<div class="profile_details">		
					<ul>
						<li class="dropdown profile_details_drop">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<div class="profile_img">	
									<div class="user-name">
										<p><?= $_SESSION["nama"] ?></p>
										<span><?= $_SESSION["level"] ?></span>
									</div>
									<i class="fa fa-angle-down lnr"></i>
									<i class="fa fa-angle-up lnr"></i>
									<div class="clearfix"></div>	
								</div>	
							</a>
							<ul class="dropdown-menu drp-mnu"><?= $_SESSION["level"] ?>
								<li> <a href="<?= site_url("login/logout") ?>"><i class="fa fa-sign-out"></i> Logout</a> </li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="clearfix"> </div>				
			</div>
			<div class="clearfix"> </div>	
		</div>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
<?PHP if(isset($_SESSION["pemberitahuan"])) {
?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">Pemberitahuan</h3>
      </div>
      <div class="modal-body">
        <h4><?= $_SESSION["pemberitahuan"] ?></h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#myModal').modal('show');
    });
</script>
<?PHP
}
?>