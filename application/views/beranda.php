<div class="col_3">
  <div class="col-md-3 widget widget1">
    <div class="r3_counter_box">
      <i class="pull-left fa fa-dollar icon-rounded"></i>
      <div class="stats">
        <h5><strong><?= $barang ?></strong></h5>
        <span>Barang</span>
      </div>
    </div>
  </div>
  <div class="col-md-3 widget widget1">
    <div class="r3_counter_box">
      <i class="pull-left fa fa-laptop user1 icon-rounded"></i>
      <div class="stats">
        <h5><strong><?= $suplier ?></strong></h5>
        <span>Suplier</span>
      </div>
    </div>
  </div>
  <div class="col-md-3 widget widget1">
    <div class="r3_counter_box">
      <i class="pull-left fa fa-money user2 icon-rounded"></i>
      <div class="stats">
        <h5><strong><?= $transaksi ?></strong></h5>
        <span>Transaksi</span>
      </div>
    </div>
  </div>
  <div class="col-md-3 widget widget1">
    <div class="r3_counter_box">
      <i class="pull-left fa fa-pie-chart dollar1 icon-rounded"></i>
      <div class="stats">
        <h5><strong><?= $pengadaanbarang ?></strong></h5>
        <span>Pengadaan Barang</span>
      </div>
    </div>
  </div>
  <div class="col-md-3 widget">
    <div class="r3_counter_box">
      <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
      <div class="stats">
        <h5><strong><?= $user ?></strong></h5>
        <span>Users</span>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<hr>
<h2 class="title1">Persediaan Barang</h2>
<div class="grids widget-shadow">
  <table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
    <thead>
      <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Stok Barang</th>
        <th>Stok Pengaman</th>
        <th>ROP</th>
        <th>Kesediaan</th>
      </tr>
    </thead>
    <tbody>
<?PHP
  foreach ($persediaanbarang as $data) {
    if($data->stok_barang == 0){
      $kesediaan = "<td class='w3-black'>Stok Habis</td>";
    }
    if(cekdataeoq($data->kd_barang)) {
      $rop = ambilrop($data->kd_barang);
      if($rop->rop >= $data->stok_barang) {
        $kesediaan = "<td>".$rop->rop."</td><td class='w3-red'>Stok Mulai Menipis</td>";
      } else {
        $kesediaan = "<td>".$rop->rop."</td><td class='w3-green'>Stok Masih Stabil</td>";
      }
    } else {
      if($data->stok_barang == 0){
        $kesediaan = "<td>Belum Pernah Dilakukan Proses EOQ</td><td class='w3-black'>Stok Habis</td>";
      } else {
        $kesediaan = "<td colspan='2' class='w3-black'>Belum Pernah Dilakukan Proses EOQ</td>";
      }
    }
?>
      <tr>
        <td><?= $data->kd_barang ?></td>
        <td><?= namabarang($data->kd_barang)?></td>
        <td><?= $data->stok_barang ?></td>
        <td><?= $data->stok_pengaman ?></td>
        <?= $kesediaan ?>
      </tr>
<?PHP
  }
?>
    </tbody>
  </table>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tabelku').DataTable();
} );
</script>
</div>