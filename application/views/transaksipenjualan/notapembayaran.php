<div class="w3-white grids" id="yangdiprint">
<h1 class="text-center w3-text-black">CAHAYA LATOLING</h1>
<h5 class="text-center w3-text-black">Toko Alat Pertanian, Alat Rumah Tangga dan Material Alat Bilah Pedang</h5>
<h6 class="text-center w3-text-black">Jl. Pemukiman No. 218 RT 1/RW 2 Lingkungan Kelurahan Massepe, Kecamatan Tellu Limpoe Kabupaten Sidenreng Rappang</h6>
<hr>
<h2 class="title1 text-center w3-text-black">NOTA PEMBELIAN</h2>
<div class="grids">
	<table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
		<tr>
			<th>Kode Transaksi</th>
			<td colspan="4"><?= $transaksipenjualan->kd_transaksi ?></td>
		</tr>
		<tr>
			<th>Tanggal Transaksi</th>
			<td colspan="4"><?= $transaksipenjualan->tgl_transaksi ?>
		</tr>
		<tr>
			<th>Kode Barang</th>
			<th>Nama Barang</th>
			<th>Jumlah</th>
			<th>Harga</th>
			<th>Total</th>
		</tr>
		<tr>
			<td><?= $transaksipenjualan->kd_barang ?></td>
			<td><?= namabarang($transaksipenjualan->kd_barang)?></td>
			<td><?= $transaksipenjualan->jumlah ?></td>
			<td><?= rupiah($transaksipenjualan->harga) ?></td>
			<td><?= rupiah($transaksipenjualan->total) ?></td>
		</tr>
		<tr>
			<th colspan="4">Bayar</th>
			<td><?= rupiah($bayar) ?></td>
		</tr>
		<tr>
			<th colspan="4">Kembali</th>
			<td><?= rupiah($kembali) ?></td>
		</tr>
	</table>
</div>
</div>
<?PHP if ($print == true) {
?>
<script type="text/javascript">
	window.onload = function () {
    window.print();
}
</script>
<?PHP
} else {
?>
<a href="<?= site_url("transaksipenjualan/nota/".$transaksipenjualan->kd_transaksi."/".$bayar."/print") ?>" class="btn btn-primary" >Print</a>
<?PHP
}
?>