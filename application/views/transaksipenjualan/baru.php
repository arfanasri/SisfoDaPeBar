<h2 class="title1">Transaksi Penjualan</h2>
<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
	<div class="form-title">
		<h4>Transaksi Baru :</h4>
	</div>
	<div class="form-body">
		<?= validation_errors()?>
		<?= form_open() ?>
			<div class="form-group">
				<label for="kd_transaksi">Kode Transaksi</label> <span onclick="kodetransaksi()" class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
				<input type="text" class="form-control" id="kd_transaksi" name="kd_transaksi" placeholder="Kode Transaksi" required="">
			</div>
			<div class="form-group">
				<label for="tgl_transaksi">Tanggal Transaksi</label>
				<input type="date" class="form-control" id="tgl_transaksi" name="tgl_transaksi" placeholder="Tanggal Transaksi" required="">
			</div>
			<div class="form-group">
				<label for="kd_barang">Barang</label>
				<select class="form-control" id="kd_barang" name="kd_barang" onchange="gantiharga()">
<?PHP
	foreach ($barang as $data) {
?>
					<option value="<?= $data->kd_barang ?>"><?= $data->kd_barang. " - " . $data->nama_barang?></option>
<?PHP
	}
?>
				</select>
			</div>
			<div class="form-group">
				<label for="jumlah">Jumlah</label>
				<input type="number" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" onChange="kali()" required="">
			</div>
			<div class="form-group">
				<label for="harga">Harga</label>
				<input type="number" class="form-control" id="harga" name="harga" placeholder="Harga" readonly="" required="">
			</div>
			<div class="form-group">
				<label for="total">Total</label>
				<input type="number" class="form-control" id="total" name="total" placeholder="Total" readonly="" required>
			</div>
			<div class="form-group">
				<label for="bayar">Bayar</label>
				<input type="number" class="form-control" id="bayar" name="bayar" placeholder="Bayar" required="" onChange="kembalian()">
			</div>
			<div class="form-group">
				<label for="kembali">Kembali</label>
				<input type="number" class="form-control" id="kembali" name="kembali" placeholder="Kembali" readonly="" required>
			</div>
			<input type="submit" class="btn btn-primary" value="Simpan">
		</form> 
	</div>
</div>
<script type="text/javascript">

	window.onload = function () {
    	gantiharga()
	}

	function kali(){
		var a = document.getElementById("jumlah").value;
		var b = document.getElementById("harga").value;
		var c = a * b;
		document.getElementById("total").value = c;
	}

	function gantiharga(){
		var a = document.getElementById("kd_barang").value;
		var hargajual = [];
	<?php foreach ($barang as $data): ?>
		hargajual["<?= $data->kd_barang ?>"] = "<?= $data->harga_jual ?>";
	<?php endforeach ?>
		document.getElementById("harga").value = hargajual[a];
	}

	function kodetransaksi(){
		var j = "tran<?= $tran ?>";

		document.getElementById("kd_transaksi").value = j;
	}

	function kembalian(){
		var total = document.getElementById("total").value;
		var bayar = document.getElementById("bayar").value;

		var kembali = bayar - total;

		document.getElementById("kembali").value = kembali;
	}
</script>