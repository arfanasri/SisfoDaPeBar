<h2 class="title1">Transaksi Penjualan</h2>
<div class="grids widget-shadow">
	<table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
		<thead>
			<tr>
				<th>Kode Transaksi</th>
				<th>Tanggal Transaksi</th>
				<th>Barang</th>
				<th>Jumlah</th>
				<th>Harga</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
<?PHP
	foreach ($transaksipenjualan as $data) {
?>
			<tr>
				<td><?= $data->kd_transaksi ?></td>
				<td><?= $data->tgl_transaksi ?></td>
				<td><?= $data->kd_barang ." - ". namabarang($data->kd_barang) ?></td>
				<td><?= $data->jumlah ?></td>
				<td><?= rupiah($data->harga) ?></td>
				<td><?= rupiah($data->total) ?></td>
			</tr>
<?PHP
	}
?>
		</tbody>
	</table>

<script type="text/javascript">
	$(document).ready(function() {
    $('#tabelku').DataTable();
} );
</script>
</div>