<h2 class="title1">Barang</h2>
<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
	<div class="form-title">
		<h4>Barang Baru :</h4>
	</div>
	<div class="form-body">
		<?= validation_errors()?>
		<?= form_open() ?>
			<div class="form-group">
				<label for="kd_barang">Kode Barang</label>
				<input type="text" class="form-control" id="kd_barang" name="kd_barang" placeholder="Kode Barang" readonly="" value="<?= $barang->kd_barang ?>">
			</div>
			<div class="form-group">
				<label for="jenis_barang">Jenis Barang</label>
				<select class="form-control" id="jenis_barang" name="jenis_barang">
					<option <?PHP if($barang->jenis_barang == 1) echo "selected" ?> value="1">Material Alat Bilah Pedang</option>
					<option <?PHP if($barang->jenis_barang == 2) echo "selected" ?> value="2">Alat Pertanian</option>
					<option <?PHP if($barang->jenis_barang == 3) echo "selected" ?> value="3">Alat Rumah Tangga</option>
				</select>
			</div>
			<div class="form-group">
				<label for="nama_barang">Nama Barang</label>
				<input type="text" class="form-control" id="nama_barang" name="nama_barang" placeholder="Nama Barang" value="<?= $barang->nama_barang ?>">
			</div>
			<div class="form-group">
				<label for="satuan">Satuan</label>
				<input type="number" class="form-control" id="satuan" name="satuan" placeholder="Satuan" value="<?= $barang->satuan ?>">
			</div>
			<div class="form-group">
				<label for="harga_jual">Harga Jual</label>
				<input type="number" class="form-control" id="harga_jual" name="harga_jual" placeholder="Harga Jual" value="<?= $barang->harga_jual ?>">
			</div>
			<div class="form-group">
				<label for="biaya_simpan">% Biaya Simpan</label>
				<input type="number" class="form-control" id="biaya_simpan" name="biaya_simpan" placeholder="% Biaya Simpan" value="<?= $barang->biaya_simpan ?>">
			</div>
			<div class="form-group">
				<label for="lead_time">Lead Time</label>
				<input type="number" class="form-control" id="lead_time" name="lead_time" placeholder="Lead Time" value="<?= $barang->lead_time ?>">
			</div>
			<input type="hidden" class="btn btn-primary" name="id" value="<?= $barang->kd_barang ?>">
			<input type="submit" class="btn btn-primary" value="Simpan">
		</form> 
	</div>
</div>