<h2 class="title1">Barang</h2>
<div class="grids widget-shadow">
	<table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
		<thead>
			<tr>
				<th>Kode Barang</th>
				<th>Jenis Barang</th>
				<th>Nama Barang</th>
				<th>Satuan</th>
				<th>Harga Jual</th>
				<th>% Biaya Simpan</th>
				<th>Lead Time</th>
				<?PHP if($_SESSION["level"] == "admin") { ?>
				<th>Alat</th>
				<?PHP } ?>
			</tr>
		</thead>
		<tbody>
<?PHP
	foreach ($barang as $data) {
?>
			<tr>
				<td><?= $data->kd_barang ?></td>
				<td><?= jenbar($data->jenis_barang) ?></td>
				<td><?= $data->nama_barang ?></td>
				<td><?= $data->satuan ?></td>
				<td><?= rupiah($data->harga_jual) ?></td>
				<td><?= $data->biaya_simpan ?> %</td>
				<td><?= $data->lead_time ?></td>
				<?PHP if($_SESSION["level"] == "admin") { ?>
				<td>
					<a href="<?= site_url("barang/ubah/".$data->kd_barang) ?>">Ubah</a>
					<a href="<?= site_url("barang/hapus/".$data->kd_barang) ?>" onclick="return confirm('Apakah anda ingin menghapus?')">Hapus</a>
				</td>
				<?PHP } ?>
			</tr>
<?PHP
	}
?>
		</tbody>
	</table>

<script type="text/javascript">
	$(document).ready(function() {
    $('#tabelku').DataTable();
} );
</script>
</div>