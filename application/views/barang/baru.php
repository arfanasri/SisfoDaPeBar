<h2 class="title1">Barang</h2>
<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
	<div class="form-title">
		<h4>Barang Baru :</h4>
	</div>
	<div class="form-body">
		<?= validation_errors()?>
		<?= form_open() ?>
			<div class="form-group">
				<label for="kd_barang">Kode Barang</label> <span onclick="kodebarang()" class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
				<input type="text" class="form-control" id="kd_barang" name="kd_barang" placeholder="Kode Barang" required="">
			</div>
			<div class="form-group">
				<label for="jenis_barang">Jenis Barang</label>
				<select class="form-control" id="jenis_barang" name="jenis_barang" onchange="kodebarang()">
					<option value="1">Material Alat Bilah Pedang</option>
					<option value="2">Alat Pertanian</option>
					<option value="3">Alat Rumah Tangga</option>
				</select>
			</div>
			<div class="form-group">
				<label for="nama_barang">Nama Barang</label>
				<input type="text" class="form-control" id="nama_barang" name="nama_barang" placeholder="Nama Barang" required>
			</div>
			<div class="form-group">
				<label for="satuan">Satuan</label>
				<input type="text" class="form-control" id="satuan" name="satuan" placeholder="Satuan" required>
			</div>
			<div class="form-group">
				<label for="harga_jual">Harga Jual</label>
				<input type="number" class="form-control" id="harga_jual" name="harga_jual" placeholder="Harga Jual" required>
			</div>
			<div class="form-group">
				<label for="biaya_simpan">% Biaya Simpan</label>
				<input type="number" class="form-control" id="biaya_simpan" name="biaya_simpan" placeholder="% Biaya Simpan" required max="100">
			</div>
			<div class="form-group">
				<label for="lead_time">Lead Time</label>
				<input type="number" class="form-control" id="lead_time" name="lead_time" placeholder="Lead Time" required>
			</div>
			<input type="submit" class="btn btn-primary" value="Simpan">
		</form> 
	</div>
</div>
<script type="text/javascript">
	window.onload = function () {
    	kodebarang();
	}

	function kodebarang(){
		var a = document.getElementById("jenis_barang").value;
		var j;
		if(a == "1"){
			j = "mabp<?= $mabp ?>";
		} else if (a == "2") {
			j = "alpe<?= $alpe ?>";
		} else if (a == "3") {
			j = "alrt<?= $alrt ?>";
		}

		document.getElementById("kd_barang").value = j;
	}
</script>