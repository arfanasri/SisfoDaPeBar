<h2 class="title1">Suplier</h2>
<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
	<div class="form-title">
		<h4>Suplier Baru :</h4>
	</div>
	<div class="form-body">
		<?= validation_errors()?>
		<?= form_open() ?>
			<div class="form-group">
				<label for="id_suplier">ID Suplier</label> <span onclick="acak()" class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
				<input type="text" class="form-control" id="id_suplier" name="id_suplier" placeholder="ID Suplier" required="">
			</div>
			<div class="form-group">
				<label for="nama_suplier">Nama Suplier</label>
				<input type="text" class="form-control" id="nama_suplier" name="nama_suplier" placeholder="Nama Suplier" required="">
			</div>
			<div class="form-group">
				<label for="alamat">Alamat</label>
				<textarea class="form-control" id="alamat" name="alamat"></textarea>
			</div>
			<div class="form-group">
				<label for="no_hp">NO HP</label>
				<input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="NO HP" required>
			</div>
			<input type="submit" class="btn btn-primary" value="Simpan">
		</form> 
	</div>
</div>
<script type="text/javascript">
	window.onload = function () {
    	acak();
	}

	function acak(){
		var sa = Math.floor((Math.random() * 10));
		var du = Math.floor((Math.random() * 10));
		var ti = Math.floor((Math.random() * 10));
		var em = Math.floor((Math.random() * 10));
		var li = Math.floor((Math.random() * 10));
		var en = Math.floor((Math.random() * 10));

		document.getElementById("id_suplier").value = "supl" + sa.toString() + du.toString() + ti.toString() + em.toString() + li.toString() + en.toString();
	}
</script>