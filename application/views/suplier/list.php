<h2 class="title1">User</h2>
<div class="grids widget-shadow">
	<table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
		<thead>
			<tr>
				<th>ID Suplier</th>
				<th>Nama Suplier</th>
				<th>Alamat</th>
				<th>NO HP</th>
				<?PHP if($_SESSION["level"] == "admin") { ?>
				<th>Alat</th>
				<?PHP } ?>
			</tr>
		</thead>
		<tbody>
<?PHP
	foreach ($suplier as $data) {
?>
			<tr>
				<td><?= $data->id_suplier ?></td>
				<td><?= $data->nama_suplier ?></td>
				<td><?= $data->alamat ?></td>
				<td><?= $data->no_hp ?></td>
				<?PHP if($_SESSION["level"] == "admin") { ?>
				<td>
					<a href="<?= site_url("suplier/ubah/".$data->id_suplier) ?>">Ubah</a>
					<a href="<?= site_url("suplier/hapus/".$data->id_suplier) ?>" onclick="return confirm('Apakah anda ingin menghapus?')">Hapus</a>
				</td>
				<?PHP } ?>
			</tr>
<?PHP
	}
?>
		</tbody>
	</table>

<script type="text/javascript">
	$(document).ready(function() {
    $('#tabelku').DataTable();
} );
</script>
</div>