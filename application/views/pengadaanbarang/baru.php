<h2 class="title1">Pengadaan Barang</h2>
<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
	<div class="form-title">
		<h4>Biaya Persediaan Baru :</h4>
	</div>
	<div class="form-body">
		<?= validation_errors()?>
		<?= form_open() ?>
			<div class="form-group">
				<label for="kd_pengadaan">Kode Pengadaan</label> <span onclick="kodepengadaan()" class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
				<input type="text" class="form-control" id="kd_pengadaan" name="kd_pengadaan" placeholder="Kode Pengadaan" required="" readonly="">
			</div>
			<div class="form-group">
				<label for="tgl_pengadaan">Tanggal Pengadaan</label>
				<input type="date" class="form-control" id="tgl_pengadaan" name="tgl_pengadaan" placeholder="Tanggal Pengadaan" required="">
			</div>
			<div class="form-group">
				<label for="id_suplier">Suplier</label>
				<select class="form-control" id="id_suplier" name="id_suplier">
<?PHP
	foreach ($suplier as $data) {
?>
					<option value="<?= $data->id_suplier ?>"><?= $data->id_suplier . " - " . $data->nama_suplier ?></option>
<?PHP
	}
?>
				</select>
			</div>
			<div class="form-group">
				<label for="kd_barang">Barang</label>
				<select class="form-control" id="kd_barang" name="kd_barang" onclick="databarang()">
<?PHP
	foreach ($barang as $data) {
?>
					<option value="<?= $data->kd_barang ?>"><?= $data->kd_barang . " - " . $data->nama_barang ?></option>
<?PHP
	}
?>
				</select>
			</div>
			<div class="form-group">
				<label for="biaya_simpan">% Biaya Simpan</label>
				<input type="number" class="form-control" id="biaya_simpan" name="biaya_simpan" placeholder="% Biaya Simpan" required="" readonly="">
			</div>
			<div class="form-group">
				<label for="harga_beli">Harga Beli</label>
				<input type="number" class="form-control" id="harga_beli" name="harga_beli" placeholder="Harga Beli" required="" onchange="totalbiaya()">
			</div>
			<div class="form-group">
				<label for="stok_permintaan">Stok Permintaan</label>
				<input type="number" class="form-control" id="stok_permintaan" name="stok_permintaan" placeholder="Stok Permintaan" required="" onchange="totalbiaya()">
			</div>
			<div class="form-group">
				<label for="biaya_pemesanan">Biaya Pemesanan</label>
				<input type="number" class="form-control" id="biaya_pemesanan" name="biaya_pemesanan" placeholder="Biaya Pemesanan" required="" onchange="totalbiaya()">
			</div>
			<div class="form-group">
				<label for="total_biaya">Total Biaya</label>
				<input type="number" class="form-control" id="total_biaya" name="total_biaya" placeholder="Total Biaya" required="" readonly="">
			</div>
			<input type="submit" class="btn btn-primary" value="Simpan">
		</form> 
	</div>
</div>
<script type="text/javascript">
	function databarang() {
		var id = document.getElementById("kd_barang").value;
		var biaya_simpan = [];
		<?php foreach ($barang as $data): ?>
			biaya_simpan["<?= $data->kd_barang ?>"] = "<?= $data->biaya_simpan ?>";
		<?php endforeach ?>
		document.getElementById("biaya_simpan").value = biaya_simpan[id];
	}

	function totalbiaya() {
		var harga_beli = document.getElementById("harga_beli").value;
		var stok_permintaan = document.getElementById("stok_permintaan").value;
		var biaya_pemesanan = document.getElementById("biaya_pemesanan").value;

		var total_biaya = parseInt(harga_beli) * parseInt(stok_permintaan);
		total_biaya = parseInt(total_biaya) + parseInt(biaya_pemesanan);

		document.getElementById("total_biaya").value = total_biaya.toString();
	}

	function kodepengadaan(){
		var j = "peng<?= $peng ?>";

		document.getElementById("kd_pengadaan").value = j;
	}
</script>