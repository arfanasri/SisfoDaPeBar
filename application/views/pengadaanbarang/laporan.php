<div class="w3-white grids" id="yangdiprint">
	<table>
		<tr>
			<td style="width: 10%"><img src="<?= base_url() ?>assets/gambar/logo.png" style="width: 100%"></td>
			<td>
<h1 class="text-center w3-text-black">CAHAYA LATOLING</h1>
<h5 class="text-center w3-text-black">Toko Alat Pertanian, Alat Rumah Tangga dan Material Alat Bilah Pedang</h5>
<h6 class="text-center w3-text-black">Jl. Pemukiman No. 218 RT 1/RW 2 Lingkungan Kelurahan Massepe, Kecamatan Tellu Limpoe Kabupaten Sidenreng Rappang</h6>
			</td>
			<td style="width: 10%"></td>
		</tr>
	</table>
<hr>
<h2 class="title1 text-center w3-text-black">DATA PENGADAAN BARANG</h2>
<div class="grids">
	<table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
		<thead>
			<tr class="w3-black">
				<th>No</th>
				<th>Kode Pengadaan</th>
				<th>Tanggal Pengadaan</th>
				<th>Suplier</th>
				<th>Barang</th>
				<th>% Biaya Simpan</th>
				<th>Harga Beli</th>
				<th>Stok Permintaan</th>
				<th>Biaya Pemesanan</th>
				<th>Total Biaya</th>
			</tr>
		</thead>
		<tbody>
<?PHP
	$no = 1;
	$totalharga = 0;
	foreach ($pengadaanbarang as $data) {
		$totalharga += $data->total_biaya;
?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $data->kd_pengadaan ?></td>
				<td><?= $data->tgl_pengadaan ?></td>
				<td><?= $data->id_suplier . " - " . namasuplier($data->id_suplier)?></td>
				<td><?= $data->kd_barang . " - " . namabarang($data->kd_barang)?></td>
				<td><?= $data->biaya_simpan ?> %</td>
				<td><?= rupiah($data->harga_beli) ?></td>
				<td><?= $data->stok_permintaan?></td>
				<td><?= rupiah($data->biaya_pemesanan) ?></td>
				<td><?= rupiah($data->total_biaya) ?></td>
			</tr>
<?PHP
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="9" class="text-right w3-black">TOTAL :</td>
				<td><?= rupiah($totalharga) ?></td>
			</tr>
		</tfoot>
	</table>
</div>
</div>
<?PHP if ($print == true) {
?>
<script type="text/javascript">
	window.onload = function () {
    window.print();
}
</script>
<?PHP
} else {
?>
<a href="<?= site_url("laporan/datapengadaanbarang/".$tahun."/".$bulan."/print") ?>" class="btn btn-primary" >Print</a>
<?PHP
}
?>