<h2 class="title1">Pengedaan Barang</h2>
<div class="grids widget-shadow">
	<table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
		<thead>
			<tr>
				<th>Kode Pengadaan</th>
				<th>Tanggal Pengadaan</th>
				<th>Suplier</th>
				<th>Barang</th>
				<th>% Biaya Simpan</th>
				<th>Harga Beli</th>
				<th>Stok Permintaan</th>
				<th>Biaya Pemesanan</th>
				<th>Total Biaya</th>
			</tr>
		</thead>
		<tbody>
<?PHP
	foreach ($pengadaanbarang as $data) {
?>
			<tr>
				<td><?= $data->kd_pengadaan ?></td>
				<td><?= $data->tgl_pengadaan ?></td>
				<td><?= $data->id_suplier . " - " . namasuplier($data->id_suplier)?></td>
				<td><?= $data->kd_barang . " - " . namabarang($data->kd_barang)?></td>
				<td><?= $data->biaya_simpan ?> %</td>
				<td><?= rupiah($data->harga_beli) ?></td>
				<td><?= $data->stok_permintaan?></td>
				<td><?= rupiah($data->biaya_pemesanan) ?></td>
				<td><?= rupiah($data->total_biaya) ?></td>
			</tr>
<?PHP
	}
?>
		</tbody>
	</table>

<script type="text/javascript">
	$(document).ready(function() {
    $('#tabelku').DataTable();
} );
</script>
</div>