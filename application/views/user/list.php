<h2 class="title1">User</h2>
<div class="grids widget-shadow">
	<table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
		<thead>
			<tr>
				<th>Username</th>
				<th>Level</th>
				<th>Alat</th>
			</tr>
		</thead>
		<tbody>
<?PHP
	foreach ($user as $data) {
?>
			<tr>
				<td><?= $data->id_user ?></td>
				<td><?= $data->level ?></td>
				<td>
					<?PHP if($data->id_user != "admin") { ?>
					<!-- <a href="<?= site_url("user/ubah/".$data->id_user) ?>">Ubah</a> -->
					<a href="<?= site_url("user/hapus/".$data->id_user) ?>" onclick="return confirm('Apakah anda ingin menghapus?')">Hapus</a>
					<?PHP } ?>
				</td>
			</tr>
<?PHP
	}
?>
		</tbody>
	</table>

<script type="text/javascript">
	$(document).ready(function() {
    $('#tabelku').DataTable();
} );
</script>
</div>