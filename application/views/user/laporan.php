<div class="w3-white grids" id="yangdiprint">
	<table>
		<tr>
			<td style="width: 10%"><img src="<?= base_url() ?>assets/gambar/logo.png" style="width: 100%"></td>
			<td>
<h1 class="text-center w3-text-black">CAHAYA LATOLING</h1>
<h5 class="text-center w3-text-black">Toko Alat Pertanian, Alat Rumah Tangga dan Material Alat Bilah Pedang</h5>
<h6 class="text-center w3-text-black">Jl. Pemukiman No. 218 RT 1/RW 2 Lingkungan Kelurahan Massepe, Kecamatan Tellu Limpoe Kabupaten Sidenreng Rappang</h6>
			</td>
			<td style="width: 10%"></td>
		</tr>
	</table>
<hr>
<h2 class="title1 text-center w3-text-black">DATA USER</h2>
<div class="grids">
	<table style="width:100%" class="table table-striped table-bordered table-hover" id="tabelku">
		<thead>
			<tr class="w3-black">
				<th>No</th>
				<th>Username</th>
				<th>Level</th>
			</tr>
		</thead>
		<tbody>
<?PHP
	$no = 1;
	foreach ($user as $data) {
?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $data->id_user ?></td>
				<td><?= $data->level ?></td>
			</tr>
<?PHP
	}
?>
		</tbody>
	</table>
</div>
</div>
<?PHP if ($print == true) {
?>
<script type="text/javascript">
	window.onload = function () {
    window.print();
}
</script>
<?PHP
} else {
?>
<a href="<?= site_url("laporan/datauser/print") ?>" class="btn btn-primary" >Print</a>
<?PHP
}
?>