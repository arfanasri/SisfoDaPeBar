<h2 class="title1">User</h2>
<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
	<div class="form-title">
		<h4>User Baru :</h4>
	</div>
	<div class="form-body">
		<?= validation_errors()?>
		<?= form_open() ?>
			<div class="form-group">
				<label for="level">Level</label>
				<select class="form-control" id="level" name="level">
					<option value="penjual">Bagian Penjualan</option>
					<option value="gudang">Bagian Gudang</option>
					<option value="admin">Admin</option>
				</select>
			</div>
			<div class="form-group">
				<label for="id_user">Username</label>
				<input type="text" class="form-control" id="id_user" name="id_user" placeholder="Username" required="">
			</div>
			<div class="form-group">
				<label for="nama_user">Nama Lengkap</label>
				<input type="text" class="form-control" id="nama_user" name="nama_user" placeholder="Nama Lengkap" required="">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
			</div>
			<input type="submit" class="btn btn-primary" value="Simpan">
		</form> 
	</div>
</div>