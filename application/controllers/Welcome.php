<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if(!isset($_SESSION["pengguna"])) {
	        $_SESSION["pengguna"] = "guest";
	        $_SESSION["level"] = "guest";
	        $_SESSION["nama"] = "Guest";
			redirect(site_url("login"));
		} elseif($_SESSION["pengguna"] == "guest") {
			redirect(site_url("login"));
		}

		$this->load->model("barang_model","bara");
		$this->load->model("suplier_model","supl");
		$this->load->model("transaksi_model","tran");
		$this->load->model("pengadaanbarang_model","peng");
		$this->load->model("user_model","usem");
		$this->load->model("eoq_model","eoqm");
		$this->load->model("persediaanbarang_model","pers");

		$data["barang"] = $this->bara->banyakdata();
		$data["suplier"] = $this->supl->banyakdata();
		$data["transaksi"] = $this->tran->banyakdata();
		$data["pengadaanbarang"] = $this->peng->banyakdata();
		$data["user"] = $this->usem->banyakdata();
		$data["persediaanbarang"] = $this->pers->ambilsemua();
		
		$this->load->view('te_at');
		$this->load->view('beranda',$data);
		$this->load->view('te_ba');
	}
}
