<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barangmasukpengaman extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
      if(!isset($_SESSION["pengguna"])) {
        $_SESSION["pengguna"] = "guest";
        $_SESSION["level"] = "guest";
        $_SESSION["nama"] = "Guest";
         redirect(site_url("login"));
      } elseif($_SESSION["pengguna"] == "guest") {
         redirect(site_url("login"));
      }
      
      $this->load->model('barangmasukpengaman_model', 'bamp');
      $this->load->model('pengadaanbarang_model', 'peng');
      $this->load->model('barang_model', 'bara');
      $this->load->model('suplier_model', 'supl');
      $this->load->model('persediaanbarang_model', 'pers');
      // Your own constructor code
   }

   public function index()
   {
      $data["barangmasukpengaman"] = $this->bamp->ambilsemua();
      $this->load->view("te_at",$data);
      $this->load->view("barangmasukpengaman/list",$data);
      $this->load->view("te_ba");
   }

   public function tambah()
   {
      $this->form_validation->set_rules('kd_barang_masuk_pengaman', 'Kode Barang Masuk', 'required');
      $this->form_validation->set_rules('tanggal', 'Tanggal Transaksi', 'required');
      $this->form_validation->set_rules('kd_pengadaan', 'Kode Pengadaan', 'required');
      $this->form_validation->set_rules('id_suplier', 'Suplier', 'required');
      $this->form_validation->set_rules('kd_barang', 'Barang', 'required');
      $this->form_validation->set_rules('biaya_simpan', '% Biaya Simpan', 'required');
      $this->form_validation->set_rules('harga_unit', 'Harga Unit', 'required');
      $this->form_validation->set_rules('biaya_pemesanan', 'Biaya Pemesanan', 'required');
      $this->form_validation->set_rules('stok', 'Stok', 'required');
      $this->form_validation->set_rules('biaya_penyimpanan', 'Biaya Penyimpanan', 'required');
      if ($this->form_validation->run() == FALSE) {

         $bamp = $this->bamp->ambildatabaru();
         $data["bamp"] = substr($bamp, 4);
         $data["bamp"] = intval($data["bamp"]) + 1;
         if($data["bamp"] < 10) {
            $data["bamp"] = "00000" . $data["bamp"];
         } elseif ($data["bamp"] < 100) {
            $data["bamp"] = "0000" . $data["bamp"];
         } elseif ($data["bamp"] < 1000) {
            $data["bamp"] = "000" . $data["bamp"];
         } elseif ($data["bamp"] < 10000) {
            $data["bamp"] = "00" . $data["bamp"];
         } elseif ($data["bamp"] < 100000) {
            $data["bamp"] = "0" . $data["bamp"];
         } elseif ($data["bamp"] < 1000000) {
            $data["bamp"] = "" . $data["bamp"];
         }

         $data["barang"] = $this->bara->ambilsemua();
         $data["suplier"] = $this->supl->ambilsemua();
         $data["pengadaan"] = $this->peng->pengadaan();
         $this->load->view("te_at");
         $this->load->view("barangmasukpengaman/baru",$data);
         $this->load->view("te_ba");
      } else {
         $this->bamp->tambahdata();
         $stok = $this->pers->ambildata($this->input->post("kd_barang"),"kd_barang");
         $masu = $this->input->post("stok");
         $stokpengaman = $stok->stok_pengaman + $masu;
         $stokbarang = $stok->stok_barang;
         $this->pers->ubahstok($this->input->post("kd_barang"),$stokbarang,$stokpengaman);
         $this->session->set_flashdata("pemberitahuan", "Berhasil Ditambahkan");
         redirect(site_url("barangmasukpengaman"));
      }
   }

   public function ubah($id)
   {
      $this->form_validation->set_rules('kd_transaksi', 'Kode Transaksi', 'required');
      $this->form_validation->set_rules('tgl_transaksi', 'Tanggal Transaksi', 'required');
      $this->form_validation->set_rules('id_suplier', 'Suplier', 'required');
      $this->form_validation->set_rules('kd_barang', 'Barang', 'required');
      $this->form_validation->set_rules('satuan', 'Satuan', 'required');
      $this->form_validation->set_rules('stok', 'Stok', 'required');
      $this->form_validation->set_rules('harga_unit', 'Harga Unit', 'required');
      $this->form_validation->set_rules('biaya_pemesanan', 'Biaya Pemesanan', 'required');
      $this->form_validation->set_rules('biaya_pengolahan', 'Biaya Pengolahan', 'required');
      if ($this->form_validation->run() == FALSE) {
         $data["barangmasuk"] = $this->bamp->ambildata($id);
         $data["barang"] = $this->bara->ambilsemua();
         $data["suplier"] = $this->supl->ambilsemua();
         $this->load->view("te_at");
         $this->load->view("barangmasuk/ubah",$data);
         $this->load->view("te_ba");
      } else {
         $this->bamp->ubahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Diubah");
         redirect(site_url("barangmasuk"));
      }
   }

   public function hapus($id)
   {
      $this->bamp->hapusdata($id);
      $this->session->set_flashdata("pemberitahuan","Berhasil Dihapus");
      redirect(site_url("barangmasukpengaman"));
   }
}