<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
      if(!isset($_SESSION["pengguna"])) {
        $_SESSION["pengguna"] = "guest";
        $_SESSION["level"] = "guest";
        $_SESSION["nama"] = "Guest";
      }

      $this->load->model('user_model', 'usem');
      // Your own constructor code
   }

   public function index()
   {
      $this->form_validation->set_rules('id_user', 'Username', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required');
      if ($this->form_validation->run() == FALSE) {
         $this->load->view("te_at");
         $this->load->view("login");
         $this->load->view("te_ba");
      } else {
         $id = $this->input->post("id_user");
         $pass = md5($this->input->post("password"));
         $level = $this->input->post("level");
         if($this->usem->cekdata($id) == true){
              if($this->usem->ceklogin($id,$pass) == true) {
                  $datauser = $this->usem->ambildata($id);
                  $this->session->set_userdata("pengguna",$datauser->id_user);
                  $this->session->set_userdata("nama",$datauser->nama_user);
                  $this->session->set_userdata("level",$datauser->level);
                  $this->session->set_flashdata("pemberitahuan","Selamat anda berhasil login");
                  redirect(site_url());
              } else {
                  $this->session->set_flashdata("pemberitahuan","Username atau Password Tidak Sesuai");
                  redirect(site_url("login"));
              }
          } else {
              $this->session->set_flashdata("pemberitahuan","Username atau Password Tidak Sesuai");
              redirect(site_url("login"));
          }
      }
   }

   public function logout(){
      $this->session->unset_userdata(array("pengguna","level"));
      $this->session->set_flashdata("pemberitahuan","Terima kasih telah berkunjung");
      redirect(site_url());
   }
}