<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eoq extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
      if(!isset($_SESSION["pengguna"])) {
        $_SESSION["pengguna"] = "guest";
        $_SESSION["level"] = "guest";
        $_SESSION["nama"] = "Guest";
         redirect(site_url("login"));
      } elseif($_SESSION["pengguna"] == "guest") {
         redirect(site_url("login"));
      }
      
      $this->load->model('eoq_model', 'eoqm');
      $this->load->model('barang_model', 'bara');
      $this->load->model('barangmasuk_model', 'bama');
      $this->load->model('transaksi_model', 'tran');
      // Your own constructor code
   }

   public function index()
   {
      $data["eoq"] = $this->eoqm->ambilsemua();
      $this->load->view("te_at",$data);
      $this->load->view("eoq/list",$data);
      $this->load->view("te_ba");
   }

   public function cekeoq() {
      $this->form_validation->set_rules('bulan', 'Bulan', 'required');
      $this->form_validation->set_rules('tahun', 'Tahun', 'required');
      $this->form_validation->set_rules('kd_barang', 'Kode Barang', 'required');
      if ($this->form_validation->run() == FALSE) {
         $data["barang"] = $this->bara->ambilsemua();
         $this->load->view("te_at");
         $this->load->view("eoq/carieoq",$data);
         $this->load->view("te_ba");
      } else {
         $tahun = $this->input->post("tahun");
         $bulan = $this->input->post("bulan");
         $barang = $this->input->post("kd_barang");
         if(cekdatabama($barang,$bulan,$tahun) && cekdatatran($barang,$bulan,$tahun)) {
            redirect(site_url("eoq/tambah/".$tahun."/".$bulan."/".$barang));
         } else {
            $this->session->set_flashdata("pemberitahuan", "Data Barang Masuk atau Barang Keluar Belum Dilengkapi");
            redirect(site_url("eoq/cekeoq"));
         }
      }
   }

   public function tambah($tahun="",$bulan="",$barang="")
   {
      $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
      $this->form_validation->set_rules('kd_barang', 'Barang', 'required');
      $this->form_validation->set_rules('harga_beli', 'Harga Beli', 'required');
      $this->form_validation->set_rules('biaya_pemesanan', 'Biaya Pemesanan', 'required');
      $this->form_validation->set_rules('biaya_penyimpanan', 'Biaya Penyimpanan', 'required');
      $this->form_validation->set_rules('lead_time', 'Lead Time', 'required');
      $this->form_validation->set_rules('permintaan', 'Permintaan', 'required');
      $this->form_validation->set_rules('eoq', 'EOQ', 'required');
      $this->form_validation->set_rules('rop', 'ROP', 'required');
      $this->form_validation->set_rules('total_biaya', 'Total Biaya', 'required');
      if ($this->form_validation->run() == FALSE) {
         $data["barang"] = $this->bara->ambildata($barang);
         $barangmasuk = $this->bama->ambilbulan($barang,$bulan,$tahun);
         $permintaan = $this->tran->ambiljumlah($barang,$bulan,$tahun);
         $data["harga_beli"] = $barangmasuk->harga_unit;
         $data["biaya_pemesanan"] = $barangmasuk->biaya_pemesanan;
         $data["biaya_penyimpanan"] = $barangmasuk->biaya_penyimpanan;
         $data["permintaan"] = $permintaan->tot;
         $data["eoq"] = round(eoq($data["permintaan"],$data["biaya_pemesanan"],$data["biaya_penyimpanan"]));
         $data["rop"] = round(rop($data["barang"]->lead_time,$data["eoq"]));
         $data["total_biaya"] = round(totalbiaya($data["permintaan"],$data["biaya_pemesanan"],$data["biaya_penyimpanan"],$data["eoq"]));
         $data["tanggal"] = $tahun."-".$bulan."-01";
         $this->load->view("te_at");
         $this->load->view("eoq/baru",$data);
         $this->load->view("te_ba");
      } else {
         $this->eoqm->tambahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Ditambahkan");
         redirect(site_url("eoq"));
      }
   }

   public function ubah($id)
   {
      $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
      $this->form_validation->set_rules('kd_barang', 'Barang', 'required');
      $this->form_validation->set_rules('harga_beli', 'Harga Beli', 'required');
      $this->form_validation->set_rules('biaya_pemesanan', 'Biaya Pemesanan', 'required');
      $this->form_validation->set_rules('biaya_penyimpanan', 'Biaya Penyimpanan', 'required');
      $this->form_validation->set_rules('lead_time', 'Lead Time', 'required');
      $this->form_validation->set_rules('permintaan', 'Permintaan', 'required');
      $this->form_validation->set_rules('eoq', 'EOQ', 'required');
      $this->form_validation->set_rules('rop', 'ROP', 'required');
      $this->form_validation->set_rules('total_biaya', 'Total Biaya', 'required');
      if ($this->form_validation->run() == FALSE) {
         $data["barang"] = $this->bara->ambilsemua();
         $data["eoqm"] = $this->eoqm->ambildata($id);
         $this->load->view("te_at");
         $this->load->view("eoq/ubah",$data);
         $this->load->view("te_ba");
      } else {
         $this->eoqm->ubahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Diubah");
         redirect(site_url("eoq"));
      }
   }

   public function hapus($id)
   {
      $this->eoqm->hapusdata($id);
      $this->session->set_flashdata("pemberitahuan","Berhasil Dihapus");
      redirect(site_url("eoq"));
   }
}