<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengadaanbarang extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
      if(!isset($_SESSION["pengguna"])) {
        $_SESSION["pengguna"] = "guest";
        $_SESSION["level"] = "guest";
        $_SESSION["nama"] = "Guest";
         redirect(site_url("login"));
      } elseif($_SESSION["pengguna"] == "guest") {
         redirect(site_url("login"));
      }
      
      $this->load->model('pengadaanbarang_model', 'peng');
      $this->load->model('barang_model', 'bara');
      $this->load->model('suplier_model', 'supl');
      // Your own constructor code
   }

   public function index()
   {
      $data["pengadaanbarang"] = $this->peng->ambilsemua();
      $this->load->view("te_at",$data);
      $this->load->view("pengadaanbarang/list",$data);
      $this->load->view("te_ba");
   }

   public function tambah()
   {
      $this->form_validation->set_rules('kd_pengadaan', 'Kode Pengadaan', 'required');
      $this->form_validation->set_rules('tgl_pengadaan', 'Tanggal Pengadaan', 'required');
      $this->form_validation->set_rules('id_suplier', 'Suplier', 'required');
      $this->form_validation->set_rules('kd_barang', 'Barang', 'required');
      $this->form_validation->set_rules('biaya_simpan', 'Barang', 'required');
      $this->form_validation->set_rules('harga_beli', 'Barang', 'required');
      $this->form_validation->set_rules('stok_permintaan', 'Stok Permintaan', 'required');
      $this->form_validation->set_rules('biaya_pemesanan', 'Biaya Pemesanan', 'required');
      $this->form_validation->set_rules('total_biaya', 'Total Biaya', 'required');
      if ($this->form_validation->run() == FALSE) {

         $peng = $this->peng->ambildatabaru();
         $data["peng"] = substr($peng, 4);
         $data["peng"] = intval($data["peng"]) + 1;
         if($data["peng"] < 10) {
            $data["peng"] = "00000" . $data["peng"];
         } elseif ($data["peng"] < 100) {
            $data["peng"] = "0000" . $data["peng"];
         } elseif ($data["peng"] < 1000) {
            $data["peng"] = "000" . $data["peng"];
         } elseif ($data["peng"] < 10000) {
            $data["peng"] = "00" . $data["peng"];
         } elseif ($data["peng"] < 100000) {
            $data["peng"] = "0" . $data["peng"];
         } elseif ($data["peng"] < 1000000) {
            $data["peng"] = "" . $data["peng"];
         }

         $data["suplier"] = $this->supl->ambilsemua();
         $data["barang"] = $this->bara->ambilsemua();
         $this->load->view("te_at");
         $this->load->view("pengadaanbarang/baru",$data);
         $this->load->view("te_ba");
      } else {
         $this->peng->tambahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Ditambahkan");
         redirect(site_url("pengadaanbarang"));
      }
   }

   public function ubah($id)
   {
      $this->form_validation->set_rules('kd_pengadaan', 'Kode Pengadaan', 'required');
      $this->form_validation->set_rules('tgl_pengadaan', 'Tanggal Pengadaan', 'required');
      $this->form_validation->set_rules('id_suplier', 'Suplier', 'required');
      $this->form_validation->set_rules('kd_barang', 'Barang', 'required');
      $this->form_validation->set_rules('biaya_simpan', 'Barang', 'required');
      $this->form_validation->set_rules('harga_beli', 'Barang', 'required');
      $this->form_validation->set_rules('stok_permintaan', 'Stok Permintaan', 'required');
      $this->form_validation->set_rules('biaya_pemesanan', 'Biaya Pemesanan', 'required');
      $this->form_validation->set_rules('total_biaya', 'Total Biaya', 'required');
      if ($this->form_validation->run() == FALSE) {
         $data["suplier"] = $this->supl->ambilsemua();
         $data["barang"] = $this->bara->ambilsemua();
         $data["pengadaanbarang"] = $this->peng->ambildata($id);
         $this->load->view("te_at");
         $this->load->view("pengadaanbarang/ubah",$data);
         $this->load->view("te_ba");
      } else {
         $this->peng->ubahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Diubah");
         redirect(site_url("pengadaanbarang"));
      }
   }

   public function hapus($id)
   {
      $this->peng->hapusdata($id);
      $this->session->set_flashdata("pemberitahuan","Berhasil Dihapus");
      redirect(site_url("pengadaanbarang"));
   }
}