<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suplier extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
      if(!isset($_SESSION["pengguna"])) {
        $_SESSION["pengguna"] = "guest";
        $_SESSION["level"] = "guest";
        $_SESSION["nama"] = "Guest";
         redirect(site_url("login"));
      } elseif($_SESSION["pengguna"] == "guest") {
         redirect(site_url("login"));
      }
      
      $this->load->model('suplier_model', 'supl');
      // Your own constructor code
   }

   public function index()
   {
      $data["suplier"] = $this->supl->ambilsemua();
      $this->load->view("te_at",$data);
      $this->load->view("suplier/list",$data);
      $this->load->view("te_ba");
   }

   public function tambah()
   {
      $this->form_validation->set_rules('id_suplier', 'ID Suplier', 'required');
      $this->form_validation->set_rules('nama_suplier', 'Nama Suplier', 'required');
      $this->form_validation->set_rules('alamat', 'Alamat', 'required');
      $this->form_validation->set_rules('no_hp', 'No HP', 'required');
      if ($this->form_validation->run() == FALSE) {
         $this->load->view("te_at");
         $this->load->view("suplier/baru");
         $this->load->view("te_ba");
      } else {
         $this->supl->tambahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Ditambahkan");
         redirect(site_url("suplier"));
      }
   }

   public function ubah($id)
   {
      $this->form_validation->set_rules('id_suplier', 'ID Suplier', 'required');
      $this->form_validation->set_rules('nama_suplier', 'Nama Suplier', 'required');
      $this->form_validation->set_rules('alamat', 'Alamat', 'required');
      $this->form_validation->set_rules('no_hp', 'No HP', 'required');
      if ($this->form_validation->run() == FALSE) {
         $data["suplier"] = $this->supl->ambildata($id);
         $this->load->view("te_at");
         $this->load->view("suplier/ubah",$data);
         $this->load->view("te_ba");
      } else {
         $this->supl->ubahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Diubah");
         redirect(site_url("suplier"));
      }
   }

   public function hapus($id)
   {
      $this->supl->hapusdata($id);
      $this->session->set_flashdata("pemberitahuan","Berhasil Dihapus");
      redirect(site_url("suplier"));
   }
}