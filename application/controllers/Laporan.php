<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
      if(!isset($_SESSION["pengguna"])) {
        $_SESSION["pengguna"] = "guest";
        $_SESSION["level"] = "guest";
        $_SESSION["nama"] = "Guest";
         redirect(site_url("login"));
      } elseif($_SESSION["pengguna"] == "guest") {
         redirect(site_url("login"));
      }

      $this->load->model('barang_model', 'bara');
      $this->load->model('barangmasuk_model', 'bama');
      $this->load->model('barangmasukpengaman_model', 'bamp');
      $this->load->model('eoq_model', 'eoqm');
      $this->load->model('pengadaanbarang_model', 'peng');
      $this->load->model('Persediaanbarang_model', 'pers');
      $this->load->model('suplier_model', 'supl');
      $this->load->model('transaksi_model', 'tran');
      $this->load->model('user_model', 'user');
      // Your own constructor code
   }

   public function databarang($print = "")
   {
      if($print == "print") {
         $data["print"] = true;
         $data["barang"] = $this->bara->ambilsemua();
         $this->load->view("print_te_at");
         $this->load->view("barang/laporan",$data);
         $this->load->view("print_te_ba");
      } else {
         $data["print"] = false;
         $data["barang"] = $this->bara->ambilsemua();
         $this->load->view("te_at",$data);
         $this->load->view("barang/laporan",$data);
         $this->load->view("te_ba");
      }
   }

   public function datasuplier($print = "")
   {
      if($print == "print") {
         $data["print"] = true;
         $data["suplier"] = $this->supl->ambilsemua();
         $this->load->view("print_te_at");
         $this->load->view("suplier/laporan",$data);
         $this->load->view("print_te_ba");
      } else {
         $data["print"] = false;
         $data["suplier"] = $this->supl->ambilsemua();
         $this->load->view("te_at",$data);
         $this->load->view("suplier/laporan",$data);
         $this->load->view("te_ba");
      }
   }

   public function datauser($print = "")
   {
      if($print == "print") {
         $data["print"] = true;
         $data["user"] = $this->user->ambilsemua();
         $this->load->view("print_te_at");
         $this->load->view("user/laporan",$data);
         $this->load->view("print_te_ba");
      } else {
         $data["print"] = false;
         $data["user"] = $this->user->ambilsemua();
         $this->load->view("te_at",$data);
         $this->load->view("user/laporan",$data);
         $this->load->view("te_ba");
      }
   }

   public function datapersediaanbarang($print = "")
   {
      if($print == "print") {
         $data["print"] = true;
         $data["persediaanbarang"] = $this->pers->ambilsemua();
         $this->load->view("print_te_at");
         $this->load->view("persediaanbarang/laporan",$data);
         $this->load->view("print_te_ba");
      } else {
         $data["print"] = false;
         $data["persediaanbarang"] = $this->pers->ambilsemua();
         $this->load->view("te_at",$data);
         $this->load->view("persediaanbarang/laporan",$data);
         $this->load->view("te_ba");
      }
   }

   public function databarangkeluar($tahun, $bulan, $print = "")
   {
      $data["tahun"] = $tahun;
      $data["bulan"] = $bulan;
      if($print == "print") {
         $data["print"] = true;
         $data["transaksipenjualan"] = $this->tran->ambilbulan($bulan,$tahun);
         $this->load->view("print_te_at");
         $this->load->view("transaksipenjualan/laporan",$data);
         $this->load->view("print_te_ba");
      } else {
         $data["print"] = false;
         $data["transaksipenjualan"] = $this->tran->ambilbulan($bulan,$tahun);
         $this->load->view("te_at",$data);
         $this->load->view("transaksipenjualan/laporan",$data);
         $this->load->view("te_ba");
      }
   }

   public function databarangmasuk($tahun, $bulan, $print = "")
   {
      $data["tahun"] = $tahun;
      $data["bulan"] = $bulan;
      if($print == "print") {
         $data["print"] = true;
         $data["barangmasuk"] = $this->bama->ambilbulanlaporan($bulan,$tahun);
         $this->load->view("print_te_at");
         $this->load->view("barangmasuk/laporan",$data);
         $this->load->view("print_te_ba");
      } else {
         $data["print"] = false;
         $data["barangmasuk"] = $this->bama->ambilbulanlaporan($bulan,$tahun);
         $this->load->view("te_at",$data);
         $this->load->view("barangmasuk/laporan",$data);
         $this->load->view("te_ba");
      }
   }

   public function databarangmasukpengaman($tahun, $bulan, $print = "")
   {
      $data["tahun"] = $tahun;
      $data["bulan"] = $bulan;
      if($print == "print") {
         $data["print"] = true;
         $data["barangmasukpengaman"] = $this->bamp->ambilbulanlaporan($bulan,$tahun);
         $this->load->view("print_te_at");
         $this->load->view("barangmasukpengaman/laporan",$data);
         $this->load->view("print_te_ba");
      } else {
         $data["print"] = false;
         $data["barangmasukpengaman"] = $this->bamp->ambilbulanlaporan($bulan,$tahun);
         $this->load->view("te_at",$data);
         $this->load->view("barangmasukpengaman/laporan",$data);
         $this->load->view("te_ba");
      }
   }

   public function datapengadaanbarang($tahun, $bulan, $print = "")
   {
      $data["tahun"] = $tahun;
      $data["bulan"] = $bulan;
      if($print == "print") {
         $data["print"] = true;
         $data["pengadaanbarang"] = $this->peng->ambilbulan($bulan,$tahun);
         $this->load->view("print_te_at");
         $this->load->view("pengadaanbarang/laporan",$data);
         $this->load->view("print_te_ba");
      } else {
         $data["print"] = false;
         $data["pengadaanbarang"] = $this->peng->ambilbulan($bulan,$tahun);
         $this->load->view("te_at",$data);
         $this->load->view("pengadaanbarang/laporan",$data);
         $this->load->view("te_ba");
      }
   }

   public function dataeoq($tahun, $bulan, $print = "")
   {
      $data["tahun"] = $tahun;
      $data["bulan"] = $bulan;
      if($print == "print") {
         $data["print"] = true;
         $data["eoq"] = $this->eoqm->ambilbulan($bulan,$tahun);
         $this->load->view("print_te_at");
         $this->load->view("eoq/laporan",$data);
         $this->load->view("print_te_ba");
      } else {
         $data["print"] = false;
         $data["eoq"] = $this->eoqm->ambilbulan($bulan,$tahun);
         $this->load->view("te_at",$data);
         $this->load->view("eoq/laporan",$data);
         $this->load->view("te_ba");
      }
   }

   public function grafik()
   {

      if(isset($_POST["kd_barang"])){
         $kd_barang = $this->input->post("kd_barang");
      } else  {
         $kd_barang = "";
      }

      $hariini = date("Y-m-d");
      $bulan[0] = date('m', strtotime('-5 month', strtotime($hariini)));
      $tahun[0] = date('Y', strtotime('-5 month', strtotime($hariini)));
      $bulan[1] = date('m', strtotime('-3 month', strtotime($hariini)));
      $tahun[1] = date('Y', strtotime('-3 month', strtotime($hariini)));
      $bulan[2] = date('m', strtotime('-2 month', strtotime($hariini)));
      $tahun[2] = date('Y', strtotime('-2 month', strtotime($hariini)));
      $bulan[3] = date('m', strtotime('-1 month', strtotime($hariini)));
      $tahun[3] = date('Y', strtotime('-1 month', strtotime($hariini)));
      $bulan[4] = date('m', strtotime('-4 month', strtotime($hariini)));
      $tahun[4] = date('Y', strtotime('-4 month', strtotime($hariini)));
      $bulan[5] = date('m');
      $tahun[5] = date('Y');

      for ($i=1; $i < 6; $i++) { 
         $data["bulan"][$i] = $bulan[$i] . " - " .$tahun[$i];
         $data["eoq"][$i] = grafikeoq($kd_barang,$bulan[$i - 1],$tahun[$i - 1]);
         $data["tanpaeoq"][$i] = grafiktanpaeoq($kd_barang,$bulan[$i],$tahun[$i]);
      }

      $data["barang"] = $this->bara->ambilsemua();

      $this->load->view("te_at",$data);
      $this->load->view("grafik/grafik",$data);
      $this->load->view("te_ba");
   }
}