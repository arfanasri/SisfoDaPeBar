<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barangmasuk extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
      if(!isset($_SESSION["pengguna"])) {
        $_SESSION["pengguna"] = "guest";
        $_SESSION["level"] = "guest";
        $_SESSION["nama"] = "Guest";
         redirect(site_url("login"));
      } elseif($_SESSION["pengguna"] == "guest") {
         redirect(site_url("login"));
      }
      
      $this->load->model('barangmasuk_model', 'bama');
      $this->load->model('pengadaanbarang_model', 'peng');
      $this->load->model('barang_model', 'bara');
      $this->load->model('suplier_model', 'supl');
      $this->load->model('persediaanbarang_model', 'pers');
      // Your own constructor code
   }

   public function index()
   {
      $data["barangmasuk"] = $this->bama->ambilsemua();
      $this->load->view("te_at",$data);
      $this->load->view("barangmasuk/list",$data);
      $this->load->view("te_ba");
   }

   public function tambah()
   {
      $this->form_validation->set_rules('kd_barang_masuk', 'Kode Barang Masuk', 'required');
      $this->form_validation->set_rules('tanggal', 'Tanggal Transaksi', 'required');
      $this->form_validation->set_rules('kd_pengadaan', 'Kode Pengadaan', 'required');
      $this->form_validation->set_rules('id_suplier', 'Suplier', 'required');
      $this->form_validation->set_rules('kd_barang', 'Barang', 'required');
      $this->form_validation->set_rules('biaya_simpan', '% Biaya Simpan', 'required');
      $this->form_validation->set_rules('harga_unit', 'Harga Unit', 'required');
      $this->form_validation->set_rules('biaya_pemesanan', 'Biaya Pemesanan', 'required');
      $this->form_validation->set_rules('stok', 'Stok', 'required');
      $this->form_validation->set_rules('biaya_penyimpanan', 'Biaya Penyimpanan', 'required');
      if ($this->form_validation->run() == FALSE) {

         $bama = $this->bama->ambildatabaru();
         $data["bama"] = substr($bama, 4);
         $data["bama"] = intval($data["bama"]) + 1;
         if($data["bama"] < 10) {
            $data["bama"] = "00000" . $data["bama"];
         } elseif ($data["bama"] < 100) {
            $data["bama"] = "0000" . $data["bama"];
         } elseif ($data["bama"] < 1000) {
            $data["bama"] = "000" . $data["bama"];
         } elseif ($data["bama"] < 10000) {
            $data["bama"] = "00" . $data["bama"];
         } elseif ($data["bama"] < 100000) {
            $data["bama"] = "0" . $data["bama"];
         } elseif ($data["bama"] < 1000000) {
            $data["bama"] = "" . $data["bama"];
         }
         
         $data["barang"] = $this->bara->ambilsemua();
         $data["suplier"] = $this->supl->ambilsemua();
         $data["pengadaan"] = $this->peng->pengadaan();
         $this->load->view("te_at");
         $this->load->view("barangmasuk/baru",$data);
         $this->load->view("te_ba");
      } else {
         $barang = $this->input->post("kd_barang");
         $tanggal = $this->input->post("tanggal");
         $bulanlalu = date('m', strtotime('-1 month', strtotime($tanggal)));
         $tahunlalu = date('Y', strtotime('-1 month', strtotime($tanggal)));

         $this->load->model("transaksi_model","tran");

         $permintaan = $this->tran->ambiljumlah($barang,$bulanlalu,$tahunlalu);

         $totalbiaya = round(totalbiaya($permintaan,$this->input->post("biaya_pemesanan"),$this->input->post("biaya_penyimpanan"),$this->input->post("stok")));

         $this->bama->tambahdata($totalbiaya);
         $stok = $this->pers->ambildata($this->input->post("kd_barang"),"kd_barang");
         $masu = $this->input->post("stok");
         $sisastok = $stok->stok_barang + $masu;
         $this->pers->ubahstok($this->input->post("kd_barang"),$sisastok);
         $this->session->set_flashdata("pemberitahuan", "Berhasil Ditambahkan");
         redirect(site_url("barangmasuk"));
      }
   }

   public function ubah($id)
   {
      $this->form_validation->set_rules('kd_transaksi', 'Kode Transaksi', 'required');
      $this->form_validation->set_rules('tgl_transaksi', 'Tanggal Transaksi', 'required');
      $this->form_validation->set_rules('id_suplier', 'Suplier', 'required');
      $this->form_validation->set_rules('kd_barang', 'Barang', 'required');
      $this->form_validation->set_rules('satuan', 'Satuan', 'required');
      $this->form_validation->set_rules('stok', 'Stok', 'required');
      $this->form_validation->set_rules('harga_unit', 'Harga Unit', 'required');
      $this->form_validation->set_rules('biaya_pemesanan', 'Biaya Pemesanan', 'required');
      $this->form_validation->set_rules('biaya_pengolahan', 'Biaya Pengolahan', 'required');
      if ($this->form_validation->run() == FALSE) {
         $data["barangmasuk"] = $this->bama->ambildata($id);
         $data["barang"] = $this->bara->ambilsemua();
         $data["suplier"] = $this->supl->ambilsemua();
         $this->load->view("te_at");
         $this->load->view("barangmasuk/ubah",$data);
         $this->load->view("te_ba");
      } else {
         $this->bama->ubahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Diubah");
         redirect(site_url("barangmasuk"));
      }
   }

   public function hapus($id)
   {
      $this->bama->hapusdata($id);
      $this->session->set_flashdata("pemberitahuan","Berhasil Dihapus");
      redirect(site_url("barangmasuk"));
   }
}