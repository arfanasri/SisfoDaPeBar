<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persediaanbarang extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
      if(!isset($_SESSION["pengguna"])) {
        $_SESSION["pengguna"] = "guest";
        $_SESSION["level"] = "guest";
        $_SESSION["nama"] = "Guest";
         redirect(site_url("login"));
      } elseif($_SESSION["pengguna"] == "guest") {
         redirect(site_url("login"));
      }
      
      $this->load->model('persediaanbarang_model', 'pers');
      $this->load->model('barang_model', 'bara');
      // Your own constructor code
   }

   public function index()
   {
      $data["persediaanbarang"] = $this->pers->ambilsemua();
      $this->load->view("te_at",$data);
      $this->load->view("persediaanbarang/list",$data);
      $this->load->view("te_ba");
   }

   public function tambah()
   {
      $this->form_validation->set_rules('bulan', 'Bulan', 'required');
      $this->form_validation->set_rules('kd_barang', 'Barang', 'required');
      $this->form_validation->set_rules('stok_barang', 'Stok Barang', 'required');
      $this->form_validation->set_rules('titik_pemesanan', 'Titik Pemesanan', 'required');
      $this->form_validation->set_rules('biaya_pengelolaan', 'Biaya Pengelolaan', 'required');
      $this->form_validation->set_rules('biaya_pemesanan', 'Biaya Pemesanan', 'required');
      if ($this->form_validation->run() == FALSE) {
         $data["barang"] = $this->bara->ambilsemua();
         $this->load->view("te_at");
         $this->load->view("persediaanbarang/baru",$data);
         $this->load->view("te_ba");
      } else {
         $this->pers->tambahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Ditambahkan");
         redirect(site_url("persediaanbarang"));
      }
   }

   public function ubah($id)
   {
      $this->form_validation->set_rules('bulan', 'Bulan', 'required');
      $this->form_validation->set_rules('kd_barang', 'Barang', 'required');
      $this->form_validation->set_rules('stok_barang', 'Stok Barang', 'required');
      $this->form_validation->set_rules('titik_pemesanan', 'Titik Pemesanan', 'required');
      $this->form_validation->set_rules('biaya_pengelolaan', 'Biaya Pengelolaan', 'required');
      $this->form_validation->set_rules('biaya_pemesanan', 'Biaya Pemesanan', 'required');
      if ($this->form_validation->run() == FALSE) {
         $data["barang"] = $this->bara->ambilsemua();
         $data["persediaanbarang"] = $this->pers->ambildata($id);
         $this->load->view("te_at");
         $this->load->view("persediaanbarang/ubah",$data);
         $this->load->view("te_ba");
      } else {
         $this->pers->ubahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Diubah");
         redirect(site_url("persediaanbarang"));
      }
   }

   public function hapus($id)
   {
      $this->pers->hapusdata($id);
      $this->session->set_flashdata("pemberitahuan","Berhasil Dihapus");
      redirect(site_url("persediaanbarang"));
   }
}