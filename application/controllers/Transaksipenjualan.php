<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksipenjualan extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
      if(!isset($_SESSION["pengguna"])) {
        $_SESSION["pengguna"] = "guest";
        $_SESSION["level"] = "guest";
        $_SESSION["nama"] = "Guest";
         redirect(site_url("login"));
      } elseif($_SESSION["pengguna"] == "guest") {
         redirect(site_url("login"));
      }
      
      $this->load->model('transaksi_model', 'tran');
      $this->load->model('barang_model', 'bara');
      $this->load->model('persediaanbarang_model', 'pers');
      // Your own constructor code
   }

   public function index()
   {
      $data["transaksipenjualan"] = $this->tran->ambilsemua();
      $this->load->view("te_at",$data);
      $this->load->view("transaksipenjualan/list",$data);
      $this->load->view("te_ba");
   }

   public function tambah()
   {
      $this->form_validation->set_rules('kd_transaksi', 'Kode Transaksi', 'required');
      $this->form_validation->set_rules('tgl_transaksi', 'Tanggal Transaksi', 'required');
      $this->form_validation->set_rules('kd_barang', 'Barang', 'required');
      $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
      $this->form_validation->set_rules('harga', 'Harga', 'required');
      $this->form_validation->set_rules('total', 'Total', 'required');
      if ($this->form_validation->run() == FALSE) {

         $tran = $this->tran->ambildatabaru();
         $data["tran"] = substr($tran, 4);
         $data["tran"] = intval($data["tran"]) + 1;
         if($data["tran"] < 10) {
            $data["tran"] = "00000" . $data["tran"];
         } elseif ($data["tran"] < 100) {
            $data["tran"] = "0000" . $data["tran"];
         } elseif ($data["tran"] < 1000) {
            $data["tran"] = "000" . $data["tran"];
         } elseif ($data["tran"] < 10000) {
            $data["tran"] = "00" . $data["tran"];
         } elseif ($data["tran"] < 100000) {
            $data["tran"] = "0" . $data["tran"];
         } elseif ($data["tran"] < 1000000) {
            $data["tran"] = "" . $data["tran"];
         }

         $data["barang"] = $this->bara->ambilsemua();
         $this->load->view("te_at");
         $this->load->view("transaksipenjualan/baru",$data);
         $this->load->view("te_ba");
      } else {
         $this->tran->tambahdata();
         $stok = $this->pers->ambildata($this->input->post("kd_barang"),"kd_barang");
         $jumlah = $this->input->post("jumlah");
         $sisastok = $stok->stok_barang - $jumlah;
         if($sisastok < 0) {
            $stokpengaman = $stok->stok_pengaman + $sisastok;
            $sisastok = 0;
         } else {
            $stokpengaman = $stok->stok_pengaman;
         }
         $this->pers->ubahstok($this->input->post("kd_barang"),$sisastok,$stokpengaman);
         $this->session->set_flashdata("pemberitahuan", "Berhasil Ditambahkan");
         redirect(site_url("transaksipenjualan/nota/".$this->input->post("kd_transaksi")."/".$this->input->post("bayar")));
      }
   }

   public function ubah($id)
   {
      $this->form_validation->set_rules('kd_transaksi', 'Kode Transaksi', 'required');
      $this->form_validation->set_rules('tgl_transaksi', 'Tanggal Transaksi', 'required');
      $this->form_validation->set_rules('kd_barang', 'Barang', 'required');
      $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
      $this->form_validation->set_rules('harga', 'Harga', 'required');
      $this->form_validation->set_rules('total', 'Total', 'required');
      if ($this->form_validation->run() == FALSE) {
         $data["barang"] = $this->bara->ambilsemua();
         $data["transaksipenjualan"] = $this->tran->ambildata($id);
         $this->load->view("te_at");
         $this->load->view("transaksipenjualan/ubah",$data);
         $this->load->view("te_ba");
      } else {
         $this->tran->ubahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Diubah");
         redirect(site_url("transaksipenjualan"));
      }
   }

   public function hapus($id)
   {
      $this->tran->hapusdata($id);
      $this->session->set_flashdata("pemberitahuan","Berhasil Dihapus");
      redirect(site_url("transaksipenjualan"));
   }

   public function nota($id,$bayar,$print = "")
   {
      $data["transaksipenjualan"] = $this->tran->ambildata($id);
      $data["bayar"] = $bayar;
      $data["kembali"] = $bayar - $data["transaksipenjualan"]->total;

      if($print == "print") {
         $data["print"] = true;
         $this->load->view("print_te_at");
         $this->load->view("transaksipenjualan/notapembayaran",$data);
         $this->load->view("print_te_ba");
      } else {
         $data["print"] = false;
         $this->load->view("te_at");
         $this->load->view("transaksipenjualan/notapembayaran",$data);
         $this->load->view("te_ba");
      }

   }
}