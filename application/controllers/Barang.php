<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
      if(!isset($_SESSION["pengguna"])) {
        $_SESSION["pengguna"] = "guest";
        $_SESSION["level"] = "guest";
        $_SESSION["nama"] = "Guest";
         redirect(site_url("login"));
      } elseif($_SESSION["pengguna"] == "guest") {
         redirect(site_url("login"));
      }
      
      $this->load->model('barang_model', 'bara');
      // Your own constructor code
   }

   public function index()
   {
      $data["barang"] = $this->bara->ambilsemua();
      $this->load->view("te_at",$data);
      $this->load->view("barang/list",$data);
      $this->load->view("te_ba");
   }

   public function tambah()
   {
      $this->form_validation->set_rules('kd_barang', 'Kode Barang', 'required');
      $this->form_validation->set_rules('jenis_barang', 'Jenis Barang', 'required');
      $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
      $this->form_validation->set_rules('satuan', 'Harga Jual', 'required');
      $this->form_validation->set_rules('harga_jual', 'Harga Jual', 'required');
      $this->form_validation->set_rules('biaya_simpan', 'Harga Jual', 'required');
      $this->form_validation->set_rules('lead_time', 'Harga Jual', 'required');
      if ($this->form_validation->run() == FALSE)
      {
         $mabp = $this->bara->ambildatabaru("mabp");
         $data["mabp"] = substr($mabp, 4);
         $data["mabp"] = intval($data["mabp"]) + 1;
         if($data["mabp"] < 10) {
            $data["mabp"] = "00000" . $data["mabp"];
         } elseif ($data["mabp"] < 100) {
            $data["mabp"] = "0000" . $data["mabp"];
         } elseif ($data["mabp"] < 1000) {
            $data["mabp"] = "000" . $data["mabp"];
         } elseif ($data["mabp"] < 10000) {
            $data["mabp"] = "00" . $data["mabp"];
         } elseif ($data["mabp"] < 100000) {
            $data["mabp"] = "0" . $data["mabp"];
         } elseif ($data["mabp"] < 1000000) {
            $data["mabp"] = "" . $data["mabp"];
         }

         $alpe = $this->bara->ambildatabaru("alpe");
         $data["alpe"] = substr($alpe, 4);
         $data["alpe"] = intval($data["alpe"]) + 1;
         if($data["alpe"] < 10) {
            $data["alpe"] = "00000" . $data["alpe"];
         } elseif ($data["alpe"] < 100) {
            $data["alpe"] = "0000" . $data["alpe"];
         } elseif ($data["alpe"] < 1000) {
            $data["alpe"] = "000" . $data["alpe"];
         } elseif ($data["alpe"] < 10000) {
            $data["alpe"] = "00" . $data["alpe"];
         } elseif ($data["alpe"] < 100000) {
            $data["alpe"] = "0" . $data["alpe"];
         } elseif ($data["alpe"] < 1000000) {
            $data["alpe"] = "" . $data["alpe"];
         }

         $alrt = $this->bara->ambildatabaru("alrt");
         $data["alrt"] = substr($alrt, 4);
         $data["alrt"] = intval($data["alrt"]) + 1;
         if($data["alrt"] < 10) {
            $data["alrt"] = "00000" . $data["alrt"];
         } elseif ($data["alrt"] < 100) {
            $data["alrt"] = "0000" . $data["alrt"];
         } elseif ($data["alrt"] < 1000) {
            $data["alrt"] = "000" . $data["alrt"];
         } elseif ($data["alrt"] < 10000) {
            $data["alrt"] = "00" . $data["alrt"];
         } elseif ($data["alrt"] < 100000) {
            $data["alrt"] = "0" . $data["alrt"];
         } elseif ($data["alrt"] < 1000000) {
            $data["alrt"] = "" . $data["alrt"];
         }

         $this->load->view("te_at");
         $this->load->view("barang/baru",$data);
         $this->load->view("te_ba");
      } else {
         $this->bara->tambahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Ditambahkan");
         redirect(site_url("barang"));
      }
   }

   public function ubah($id)
   {
      $this->form_validation->set_rules('kd_barang', 'Kode Barang', 'required');
      $this->form_validation->set_rules('jenis_barang', 'Jenis Barang', 'required');
      $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
      $this->form_validation->set_rules('satuan', 'Harga Jual', 'required');
      $this->form_validation->set_rules('harga_jual', 'Harga Jual', 'required');
      $this->form_validation->set_rules('biaya_simpan', 'Harga Jual', 'required');
      $this->form_validation->set_rules('lead_time', 'Harga Jual', 'required');
      if ($this->form_validation->run() == FALSE) {
         $data["barang"] = $this->bara->ambildata($id);
         $this->load->view("te_at");
         $this->load->view("barang/ubah",$data);
         $this->load->view("te_ba");
      } else {
         $this->bara->ubahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Diubah");
         redirect(site_url("barang"));
      }
   }

   public function hapus($id)
   {
      $this->bara->hapusdata($id);
      $this->session->set_flashdata("pemberitahuan","Berhasil Dihapus");
      redirect(site_url("barang"));
   }
}