<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
      if(!isset($_SESSION["pengguna"])) {
        $_SESSION["pengguna"] = "guest";
        $_SESSION["level"] = "guest";
        $_SESSION["nama"] = "Guest";
         redirect(site_url("login"));
      } elseif($_SESSION["pengguna"] == "guest") {
         redirect(site_url("login"));
      }
      
      $this->load->model('user_model', 'usem');
      // Your own constructor code
   }

   public function index()
   {
      $data["user"] = $this->usem->ambilsemua();
      $this->load->view("te_at",$data);
      $this->load->view("user/list",$data);
      $this->load->view("te_ba");
   }

   public function tambah()
   {
      $this->form_validation->set_rules('id_user', 'Username', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required');
      $this->form_validation->set_rules('level', 'Level', 'required');
      if ($this->form_validation->run() == FALSE) {
         $this->load->view("te_at");
         $this->load->view("user/baru");
         $this->load->view("te_ba");
      } else {
         $this->usem->tambahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Ditambahkan");
         redirect(site_url("user"));
      }
   }

   public function ubah($id)
   {
      $this->form_validation->set_rules('id_user', 'Username', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required');
      $this->form_validation->set_rules('level', 'Level', 'required');
      if ($this->form_validation->run() == FALSE) {
         $data["user"] = $this->usem->ambildata($id);
         $this->load->view("te_at");
         $this->load->view("user/ubah",$data);
         $this->load->view("te_ba");
      } else {
         $this->usem->ubahdata();
         $this->session->set_flashdata("pemberitahuan", "Berhasil Diubah");
         redirect(site_url("user"));
      }
   }

   public function hapus($id)
   {
      $this->usem->hapusdata($id);
      $this->session->set_flashdata("pemberitahuan","Berhasil Dihapus");
      redirect(site_url("user"));
   }

   public function login()
   {
      $this->form_validation->set_rules('id_user', 'Username', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required');
      $this->form_validation->set_rules('level', 'Level', 'required');
      if ($this->form_validation->run() == FALSE) {
         $this->load->view("te_at");
         $this->load->view("login",$data);
         $this->load->view("te_ba");
      } else {
         $this->usem->tambahdata();
         $this->session->set_flashdata("pemberitahuan", "Selamat Anda Berhasil Login");
         redirect(site_url("user"));
      }
   }
}