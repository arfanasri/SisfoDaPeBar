<?PHP
	function jenbar($id){
	
	    if($id == 1) {
	    	$kembali = "Material Alat Bilah Pedang";
	    } elseif ($id == 2) {
	    	$kembali = "Alat Pertanian";
	    } elseif ($id == 3) {
	    	$kembali = "Alat Rumah Tangga";
	    }

	    return $kembali;

	}

	function namabarang($id){
	
	    $CI = get_instance();

	    $CI->load->model('barang_model',"bara");
	
	    $datahelper = $CI->bara->ambildata($id);

	    return $datahelper->nama_barang;

	}

	function hargabarang($id){
	
	    $CI = get_instance();

	    $CI->load->model('barang_model',"bara");
	
	    $datahelper = $CI->bara->ambildata($id);

	    return $datahelper->harga_jual;

	}

	function namasuplier($id){
	
	    $CI = get_instance();

	    $CI->load->model('suplier_model',"supl");
	
	    $datahelper = $CI->supl->ambildata($id);

	    return $datahelper->nama_suplier;

	}

	function banyakeoq($id){
	
	    $CI = get_instance();

	    $CI->load->model('eoq_model',"eoqm");
	
	    $datahelper = $CI->eoqm->ambildata($id);

	    return $datahelper->eoq;

	}

	function rupiah($id) {
		return "Rp. ". number_format($id);
	}

	function rop($leadtime,$eoq) {
		$kembali = $eoq / 30;
		$kembali = $leadtime * $kembali;
		return $kembali;
	}

	function eoq($d,$oc,$cc){
		$kembali = 2 * $d * $oc;
		$kembali = $kembali / $cc;
		$kembali = sqrt($kembali);
		return $kembali;
	}

	function totalbiaya($d,$oc,$cc,$q){
		$a = $cc * ($q / 2);
		$b = $oc * ($d / $q);
		$kembali = $a + $b;
		return $kembali;
	}

	function dataeoq($id){
	
	    $CI = get_instance();

	    $CI->load->model('eoq_model',"eoqm");
	
	    return $datahelper = $CI->eoqm->ambildata($id);

	}

	function ambilrop($id){
	
	    $CI = get_instance();

	    $CI->load->model('eoq_model',"eoqm");
	
	    return $datahelper = $CI->eoqm->ambildataterbaru($id);

	}

	function databama($id){
	
	    $CI = get_instance();

	    $CI->load->model('barangmasuk_model',"bama");
	
	    return $datahelper = $CI->bama->ambildata($id,"kd_barang");

	}

	function databake($id){
	
	    $CI = get_instance();

	    $CI->load->model('transaksi_model',"tran");
	
	    return $datahelper = $CI->tran->ambildata($id,"kd_barang");

	}

	function datapers($id){
	
	    $CI = get_instance();

	    $CI->load->model('persediaanbarang_model',"pers");
	
	    return $datahelper = $CI->pers->ambildata($id,"kd_barang");

	}

	function datarop($id){

	    $CI = get_instance();

	    $CI->load->model('eoq_model',"eoqm");
	    $CI->load->model('rop_model',"ropm");

	    $dataeoq = $CI->eoqm->ambildata($id,"kd_barang");

	    $datarop = $CI->ropm->ambildata($dataeoq->kd_eoq,"kd_eoq");

	    $kembali = $dataeoq->eoq / $datarop->lead_time;
		$kembali = $datarop->periode_pemesanan * $kembali;
	
	    return $kembali;
	}

	function cekdatabama($barang,$bulan,$tahun) {

	    $CI = get_instance();

	    $CI->load->model('barangmasuk_model',"bama");

	    if($CI->bama->cekdata($barang,$bulan,$tahun) >= 1){
	    	return true;
	    } else {
	    	return false;
	    }
		
	}

	function cekdatatran($barang,$bulan,$tahun) {

	    $CI = get_instance();

	    $CI->load->model('transaksi_model',"tran");

	    if($CI->tran->cekdata($barang,$bulan,$tahun) >= 1){
	    	return true;
	    } else {
	    	return false;
	    }

	}

	function cekdataeoq($barang) {

	    $CI = get_instance();

	    $CI->load->model('eoq_model',"eoqm");

	    if($CI->eoqm->cekdata($barang) >= 1){
	    	return true;
	    } else {
	    	return false;
	    }

	}

	function grafikeoq($id,$bulan,$tahun){
	
	    $CI = get_instance();

	    $CI->load->model('eoq_model',"eoqm");
	
	    if($CI->eoqm->ambilgrafik($id,$bulan,$tahun,"cek")) {
	    	$datahelper = $CI->eoqm->ambilgrafik($id,$bulan,$tahun);
	    	return $datahelper->total_biaya;
	    } else {
	    	return 0;
	    }


	}

	function grafiktanpaeoq($id,$bulan,$tahun){
	
	    $CI = get_instance();

	    $CI->load->model('barangmasuk_model',"bama");
		if($CI->bama->ambilgrafik($id,$bulan,$tahun,"cek")) {
	    	$datahelper = $CI->bama->ambilgrafik($id,$bulan,$tahun);
	    	return $datahelper->totalbiaya;
		} else {
			return 0;
		}


	}
?>