<?PHP
        class User_model extends CI_Model {

                public $id_user;
                public $nama_user;
                public $password;
                public $level;
                
                public function ambilsemua()
                {
                        $query = $this->db->get('user');
                        return $query->result();
                }

                public function ambildata($id)
                {
                        $this->db->where("id_user", $id);
                        $query = $this->db->get('user');
                        return $query->row();
                }

                public function tambahdata()
                {
                        $this->id_user   = $this->input->post("id_user");
                        $this->nama_user = $this->input->post("nama_user");
                        $this->password  = md5($this->input->post("password"));
                        $this->level     = $this->input->post("level");
                        
                        $this->db->insert('user', $this);
                }

                public function ubahdata()
                {
                        $this->id_user   = $this->input->post("id_user");
                        $this->nama_user = $this->input->post("nama_user");
                        $this->password  = md5($this->input->post("password"));
                        $this->level     = $this->input->post("level");

                        $this->db->update('user', $this, array('id_user' => $this->input->post("id")));
                }

                public function hapusdata($id)
                {
                        $this->db->where('id_user', $id);
                        $this->db->delete('user');
                }
                
                public function banyakdata()
                {
                        return $this->db->count_all("user");
                }

                public function cekdata($id)
                {
                        $this->db->where("id_user", $id);
                        $query = $this->db->get('user');
                        if($query->num_rows() > 0) {
                                return true;
                        } else {
                                return false;
                        }
                }

                public function ceklogin($id,$pass)
                {
                        $this->db->where("id_user", $id);
                        $query = $this->db->get('user');
                        $data = $query->row();
                        if($data->password == $pass){
                                return true;
                        } else {
                                return false;
                        }
                }

        }
?>