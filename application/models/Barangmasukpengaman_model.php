<?PHP
        class Barangmasukpengaman_model extends CI_Model {

                public $kd_barang_masuk_pengaman;
                public $tanggal;
                public $kd_pengadaan;
                public $id_suplier;
                public $kd_barang;
                public $biaya_simpan;
                public $harga_unit;
                public $biaya_pemesanan;
                public $stok;
                public $biaya_penyimpanan;

                public function ambilsemua()
                {
                        $query = $this->db->get('barang_masuk_pengaman');
                        return $query->result();
                }

                public function ambildata($id,$mana = "kd_barang_masuk_pengaman")
                {
                        $this->db->where($mana, $id);
                        $query = $this->db->get('barang_masuk_pengaman');
                        return $query->row();
                }

                public function ambildatabaru()
                {
                        $this->db->order_by("kd_barang_masuk_pengaman","desc");
                        $query = $this->db->get('barang_masuk_pengaman');
                        if($query->num_rows() > 0) {
                                $data = $query->row();
                                return $data->kd_barang_masuk_pengaman;
                        } else {
                                return "bamp000000";
                        }
                }

                public function ambilgrafik($id,$bulan,$tahun,$cek = "")
                {
                        $this->db->where("kd_barang", $id);
                        $this->db->where("MONTH(tanggal) = ", $bulan);
                        $this->db->where("YEAR(tanggal) = ", $tahun);
                        $this->db->order_by("tanggal","desc");
                        $query = $this->db->get('barang_masuk_pengaman');
                        if($cek == "cek") {
                                if($query->num_rows() > 0) {
                                        return true;
                                } else {
                                        return false;
                                }
                        } else {
                                return $query->row();
                        }
                }
                public function ambilbulan($id,$bulan,$tahun)
                {
                        $this->db->where("kd_barang", $id);
                        $this->db->where("MONTH(tanggal) = ", $bulan);
                        $this->db->where("YEAR(tanggal) = ", $tahun);
                        $query = $this->db->get('barang_masuk_pengaman');
                        return $query->row();
                }

                public function ambilbulanlaporan($bulan,$tahun)
                {
                        $this->db->where("MONTH(tanggal) = ", $bulan);
                        $this->db->where("YEAR(tanggal) = ", $tahun);
                        $query = $this->db->get('barang_masuk_pengaman');
                        return $query->result();
                }

                public function tambahdata()
                {
                        $this->kd_barang_masuk_pengaman   = $this->input->post("kd_barang_masuk_pengaman");
                        $this->tanggal           = $this->input->post("tanggal");
                        $this->kd_pengadaan      = $this->input->post("kd_pengadaan");
                        $this->id_suplier        = $this->input->post("id_suplier");
                        $this->kd_barang         = $this->input->post("kd_barang");
                        $this->biaya_simpan      = $this->input->post("biaya_simpan");
                        $this->harga_unit        = $this->input->post("harga_unit");
                        $this->biaya_pemesanan   = $this->input->post("biaya_pemesanan");
                        $this->stok              = $this->input->post("stok"); 
                        $this->biaya_penyimpanan = $this->input->post("biaya_penyimpanan");

                        $this->db->insert('barang_masuk_pengaman', $this);
                }

                public function ubahdata($totalbiaya)
                {
                        $this->kd_barang_masuk_pengaman   = $this->input->post("kd_barang_masuk_pengaman");
                        $this->tanggal           = $this->input->post("tanggal");
                        $this->kd_pengadaan      = $this->input->post("kd_pengadaan");
                        $this->id_suplier        = $this->input->post("id_suplier");
                        $this->kd_barang         = $this->input->post("kd_barang");
                        $this->biaya_simpan      = $this->input->post("biaya_simpan");
                        $this->harga_unit        = $this->input->post("harga_unit");
                        $this->biaya_pemesanan   = $this->input->post("biaya_pemesanan");
                        $this->stok              = $this->input->post("stok"); 
                        $this->biaya_penyimpanan = $this->input->post("biaya_penyimpanan");
                        $this->totalbiaya        = $totalbiaya;

                        $this->db->update('barang_masuk_pengaman', $this, array('kd_barang_masuk_pengaman' => $this->input->post("id")));
                }

                public function hapusdata($id)
                {
                        $this->db->where('kd_barang_masuk_pengaman', $id);
                        $this->db->delete('barang_masuk_pengaman');
                }

                public function cekdata($barang,$bulan,$tahun){
                        $this->db->where("kd_barang", $barang);
                        $this->db->where("MONTH(tanggal) = ", $bulan);
                        $this->db->where("YEAR(tanggal) = ", $tahun);
                        $query = $this->db->get("barang_masuk_pengaman");
                        return $query->num_rows();
                }

        }
?>