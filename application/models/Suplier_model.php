<?PHP
        class Suplier_model extends CI_Model {

                public $id_suplier;
                public $nama_suplier;
                public $alamat;
                public $no_hp;
                
                public function ambilsemua()
                {
                        $query = $this->db->get('suplier');
                        return $query->result();
                }

                public function ambildata($id)
                {
                        $this->db->where("id_suplier", $id);
                        $query = $this->db->get('suplier');
                        return $query->row();
                }

                public function tambahdata()
                {
                        $this->id_suplier   = $this->input->post("id_suplier");
                        $this->nama_suplier = $this->input->post("nama_suplier");
                        $this->alamat       = $this->input->post("alamat");
                        $this->no_hp        = $this->input->post("no_hp");
                        
                        $this->db->insert('suplier', $this);
                }

                public function ubahdata()
                {
                        $this->id_suplier   = $this->input->post("id_suplier");
                        $this->nama_suplier = $this->input->post("nama_suplier");
                        $this->alamat       = $this->input->post("alamat");
                        $this->no_hp        = $this->input->post("no_hp");

                        $this->db->update('suplier', $this, array('id_suplier' => $this->input->post("id")));
                }

                public function hapusdata($id)
                {
                        $this->db->where('id_suplier', $id);
                        $this->db->delete('suplier');
                }
                
                public function banyakdata()
                {
                        return $this->db->count_all("suplier");
                }

        }
?>