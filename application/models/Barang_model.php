<?PHP
        class Barang_model extends CI_Model {

                public $kd_barang;
                public $jenis_barang;
                public $nama_barang;
                public $satuan;
                public $harga_jual;
                public $biaya_simpan;
                public $lead_time;

                public function ambilsemua()
                {
                        $query = $this->db->get('barang');
                        return $query->result();
                }

                public function ambildata($id)
                {
                        $this->db->where("kd_barang", $id);
                        $query = $this->db->get('barang');
                        return $query->row();
                }

                public function ambildatabaru($id)
                {
                        $this->db->like("kd_barang", $id, "after");
                        $this->db->order_by("kd_barang","desc");
                        $query = $this->db->get('barang');
                        if($query->num_rows() > 0) {
                                $data = $query->row();
                                return $data->kd_barang;
                        } else {
                                return $id."000000";
                        }
                }

                public function tambahdata()
                {
                        $this->kd_barang      = $this->input->post("kd_barang");
                        $this->jenis_barang   = $this->input->post("jenis_barang");
                        $this->nama_barang    = $this->input->post("nama_barang");
                        $this->satuan         = $this->input->post("satuan");
                        $this->harga_jual     = $this->input->post("harga_jual");
                        $this->biaya_simpan   = $this->input->post("biaya_simpan");
                        $this->lead_time      = $this->input->post("lead_time");

                        $this->db->insert('barang', $this);

                        $persediaan["kd_barang"] = $this->kd_barang;
                        $this->db->insert("persediaan_barang",$persediaan);
                }

                public function ubahdata()
                {
                        $this->kd_barang      = $this->input->post("kd_barang");
                        $this->jenis_barang   = $this->input->post("jenis_barang");
                        $this->nama_barang    = $this->input->post("nama_barang");
                        $this->satuan         = $this->input->post("satuan");
                        $this->harga_jual     = $this->input->post("harga_jual");
                        $this->biaya_simpan   = $this->input->post("biaya_simpan");
                        $this->lead_time      = $this->input->post("lead_time");

                        $this->db->update('barang', $this, array('kd_barang' => $this->input->post("id")));
                }

                public function hapusdata($id)
                {
                        $this->db->where('kd_barang', $id);
                        $this->db->delete('barang');
                }

                public function banyakdata()
                {
                        return $this->db->count_all("barang");
                }

        }
?>