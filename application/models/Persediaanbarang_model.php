<?PHP
        class Persediaanbarang_model extends CI_Model {

                public $kd_barang;
                public $bulan;
                public $stok_barang;
                public $titik_pemesanan;
                public $biaya_pengelolaan;
                public $biaya_pemesanan;

                public function ubahstok($id,$stok,$stokpengaman = "") {
                        
                        if($stokpengaman == "") {
                                $data["stok_barang"] = $stok;
                        } else {
                                $data["stok_barang"] = $stok;
                                $data["stok_pengaman"] = $stokpengaman;
                        }
                        
                        $this->db->update("persediaan_barang", $data, array("kd_barang" => $id));
                }

                public function ambilsemua()
                {
                        $query = $this->db->get('persediaan_barang');
                        return $query->result();
                }

                public function ambildata($id,$mana = "id_persediaan")
                {
                        $this->db->where($mana, $id);
                        $query = $this->db->get('persediaan_barang');
                        return $query->row();
                }

                public function tambahdata()
                {
                        $this->kd_barang         = $this->input->post("kd_barang");
                        $this->bulan             = $this->input->post("bulan");
                        $this->stok_barang       = $this->input->post("stok_barang");
                        $this->titik_pemesanan   = $this->input->post("titik_pemesanan");
                        $this->biaya_pengelolaan = $this->input->post("biaya_pengelolaan");
                        $this->biaya_pemesanan   = $this->input->post("biaya_pemesanan");

                        $this->db->insert('persediaan_barang', $this);
                }

                public function ubahdata()
                {
                        $this->kd_barang         = $this->input->post("kd_barang");
                        $this->bulan             = $this->input->post("bulan");
                        $this->stok_barang       = $this->input->post("stok_barang");
                        $this->titik_pemesanan   = $this->input->post("titik_pemesanan");
                        $this->biaya_pengelolaan = $this->input->post("biaya_pengelolaan");
                        $this->biaya_pemesanan   = $this->input->post("biaya_pemesanan");

                        $this->db->update('persediaan_barang', $this, array('id_persediaan' => $this->input->post("id")));
                }

                public function hapusdata($id)
                {
                        $this->db->where('id_persediaan', $id);
                        $this->db->delete('persediaan_barang');
                }

        }
?>