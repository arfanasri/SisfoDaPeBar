<?PHP
        class Eoq_model extends CI_Model {

                public $id_eoq;
                public $tanggal;
                public $kd_barang;
                public $harga_beli;
                public $biaya_pemesanan;
                public $biaya_penyimpanan;
                public $lead_time;
                public $permintaan;
                public $eoq;
                public $rop;
                public $total_biaya;

                public function ambilsemua()
                {
                        $query = $this->db->get('tabel_eoq');
                        return $query->result();
                }

                public function ambildata($id,$mana = "id_eoq")
                {
                        $this->db->where($mana, $id);
                        $query = $this->db->get('tabel_eoq');
                        return $query->row();
                }

                public function ambilgrafik($id,$bulan,$tahun,$cek = "")
                {
                        $this->db->where("kd_barang", $id);
                        $this->db->where("MONTH(tanggal) = ", $bulan);
                        $this->db->where("YEAR(tanggal) = ", $tahun);
                        $this->db->order_by("tanggal","desc");
                        $query = $this->db->get('tabel_eoq');
                        if($cek == "cek") {
                                if($query->num_rows() > 0) {
                                        return true;
                                } else {
                                        return false;
                                }
                        } else {
                                return $query->row();
                        }
                }

                public function ambildataterbaru($id,$mana = "kd_barang")
                {
                        $this->db->where($mana, $id);
                        $this->db->order_by("tanggal","desc");
                        $query = $this->db->get('tabel_eoq');
                        return $query->row();
                }

                public function ambilbulan($bulan,$tahun)
                {
                        $this->db->where("MONTH(tanggal) = ", $bulan);
                        $this->db->where("YEAR(tanggal) = ", $tahun);
                        $query = $this->db->get('tabel_eoq');
                        return $query->result();
                }

                public function tambahdata()
                {
                        $this->id_eoq            = $this->input->post("id_eoq");
                        $this->tanggal           = $this->input->post("tanggal");
                        $this->kd_barang         = $this->input->post("kd_barang");
                        $this->harga_beli        = $this->input->post("harga_beli");
                        $this->biaya_pemesanan   = $this->input->post("biaya_pemesanan");
                        $this->biaya_penyimpanan = $this->input->post("biaya_penyimpanan");
                        $this->lead_time         = $this->input->post("lead_time");
                        $this->permintaan        = $this->input->post("permintaan");
                        $this->eoq               = $this->input->post("eoq");
                        $this->rop               = $this->input->post("rop");
                        $this->total_biaya       = $this->input->post("total_biaya");

                        $this->db->insert('tabel_eoq', $this);
                }

                public function ubahdata()
                {
                        $this->id_eoq            = $this->input->post("id_eoq");
                        $this->tanggal           = $this->input->post("tanggal");
                        $this->kd_barang         = $this->input->post("kd_barang");
                        $this->harga_beli        = $this->input->post("harga_beli");
                        $this->biaya_pemesanan   = $this->input->post("biaya_pemesanan");
                        $this->biaya_penyimpanan = $this->input->post("biaya_penyimpanan");
                        $this->lead_time         = $this->input->post("lead_time");
                        $this->permintaan        = $this->input->post("permintaan");
                        $this->eoq               = $this->input->post("eoq");
                        $this->rop               = $this->input->post("rop");
                        $this->total_biaya       = $this->input->post("total_biaya");

                        $this->db->update('tabel_eoq', $this, array('id_eoq' => $this->input->post("id")));
                }

                public function hapusdata($id)
                {
                        $this->db->where('id_eoq', $id);
                        $this->db->delete('tabel_eoq');
                }

                public function cekdata($barang){
                        $this->db->where("kd_barang", $barang);
                        $query = $this->db->get("tabel_eoq");
                        return $query->num_rows();
                }

        }
?>