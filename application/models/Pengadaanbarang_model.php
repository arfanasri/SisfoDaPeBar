<?PHP
        class Pengadaanbarang_model extends CI_Model {

                public $kd_pengadaan;
                public $tgl_pengadaan;
                public $id_suplier;
                public $kd_barang;
                public $biaya_simpan;
                public $harga_beli;
                public $stok_permintaan;
                public $biaya_pemesanan;
                public $total_biaya;
                
                public function ambilsemua()
                {
                        $query = $this->db->get('pengadaan_barang');
                        return $query->result();
                }

                public function ambildata($id)
                {
                        $this->db->where("kd_pengadaan", $id);
                        $query = $this->db->get('pengadaan_barang');
                        return $query->row();
                }

                public function ambildatabaru()
                {
                        $this->db->order_by("kd_pengadaan","desc");
                        $query = $this->db->get('pengadaan_barang');
                        if($query->num_rows() > 0) {
                                $data = $query->row();
                                return $data->kd_pengadaan;
                        } else {
                                return "peng000000";
                        }
                }

                public function ambilbulan($bulan,$tahun)
                {
                        $this->db->where("MONTH(tgl_pengadaan) = ", $bulan);
                        $this->db->where("YEAR(tgl_pengadaan) = ", $tahun);
                        $query = $this->db->get('pengadaan_barang');
                        return $query->result();
                }

                public function tambahdata()
                {
                        $this->kd_pengadaan    = $this->input->post("kd_pengadaan");
                        $this->tgl_pengadaan   = $this->input->post("tgl_pengadaan");
                        $this->id_suplier      = $this->input->post("id_suplier");
                        $this->kd_barang       = $this->input->post("kd_barang");
                        $this->biaya_simpan    = $this->input->post("biaya_simpan");
                        $this->harga_beli      = $this->input->post("harga_beli");
                        $this->stok_permintaan = $this->input->post("stok_permintaan");
                        $this->biaya_pemesanan = $this->input->post("biaya_pemesanan");
                        $this->total_biaya     = $this->input->post("total_biaya");
                        
                        $this->db->insert('pengadaan_barang', $this);
                }

                public function ubahdata()
                {
                        $this->kd_pengadaan    = $this->input->post("kd_pengadaan");
                        $this->tgl_pengadaan   = $this->input->post("tgl_pengadaan");
                        $this->id_suplier      = $this->input->post("id_suplier");
                        $this->kd_barang       = $this->input->post("kd_barang");
                        $this->biaya_simpan    = $this->input->post("biaya_simpan");
                        $this->harga_beli      = $this->input->post("harga_beli");
                        $this->stok_permintaan = $this->input->post("stok_permintaan");
                        $this->biaya_pemesanan = $this->input->post("biaya_pemesanan");
                        $this->total_biaya     = $this->input->post("total_biaya");
                        
                        $this->db->update('pengadaan_barang', $this, array('kd_pengadaan' => $this->input->post("id")));
                }

                public function hapusdata($id)
                {
                        $this->db->where('kd_pengadaan', $id);
                        $this->db->delete('pengadaan_barang');
                }
                
                public function banyakdata()
                {
                        return $this->db->count_all("pengadaan_barang");
                }

                public function pengadaan()
                {
                        $query = $this->db->query("SELECT t.* FROM pengadaan_barang AS t
                                LEFT JOIN barang_masuk AS bm ON bm.kd_pengadaan = t.kd_pengadaan
                                LEFT JOIN barang_masuk_pengaman AS bmp ON bmp.kd_pengadaan = t.kd_pengadaan
                                WHERE bm.kd_pengadaan IS NULL and bmp.kd_pengadaan IS NULL");

                        return $query->result();
                }

        }
?>