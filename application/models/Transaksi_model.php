<?PHP
        class Transaksi_model extends CI_Model {

                public $kd_transaksi;
                public $tgl_transaksi;
                public $kd_barang;
                public $jumlah;
                public $harga;
                public $total;
                
                public function ambilsemua()
                {
                        $query = $this->db->get('transaksi');
                        return $query->result();
                }

                public function ambildata($id,$mana = "kd_transaksi")
                {
                        $this->db->where($mana, $id);
                        $query = $this->db->get('transaksi');
                        return $query->row();
                }

                public function ambildatabaru()
                {
                        $this->db->order_by("kd_transaksi","desc");
                        $query = $this->db->get('transaksi');
                        if($query->num_rows() > 0) {
                                $data = $query->row();
                                return $data->kd_transaksi;
                        } else {
                                return "tran000000";
                        }
                }

                public function ambilbulan($bulan,$tahun)
                {
                        $this->db->where("MONTH(tgl_transaksi) = ", $bulan);
                        $this->db->where("YEAR(tgl_transaksi) = ", $tahun);
                        $query = $this->db->get('transaksi');
                        return $query->result();
                }

                public function ambiljumlah($id,$bulan,$tahun)
                {
                        $this->db->select_sum('jumlah',"tot");
                        $this->db->where("kd_barang", $id);
                        $this->db->where("MONTH(tgl_transaksi) = ", $bulan);
                        $this->db->where("YEAR(tgl_transaksi) = ", $tahun);
                        $query = $this->db->get('transaksi');
                        return $query->row();
                }

                public function tambahdata()
                {
                        $this->kd_transaksi  = $this->input->post("kd_transaksi");
                        $this->tgl_transaksi = $this->input->post("tgl_transaksi");
                        $this->kd_barang     = $this->input->post("kd_barang");
                        $this->jumlah        = $this->input->post("jumlah");
                        $this->harga         = $this->input->post("harga");
                        $this->total         = $this->input->post("total");
                        
                        $this->db->insert('transaksi', $this);
                }

                public function ubahdata()
                {
                        $this->kd_transaksi = $this->input->post("kd_transaksi");
                        $this->kd_barang    = $this->input->post("kd_barang");
                        $this->jumlah       = $this->input->post("jumlah");
                        $this->harga        = $this->input->post("harga");
                        $this->total        = $this->input->post("total");

                        $this->db->update('transaksi', $this, array('kd_transaksi' => $this->input->post("id")));
                }

                public function hapusdata($id)
                {
                        $this->db->where('kd_transaksi', $id);
                        $this->db->delete('transaksi');
                }
                
                public function banyakdata()
                {
                        return $this->db->count_all("transaksi");
                }

                public function cekdata($barang,$bulan,$tahun){
                        $this->db->where("kd_barang", $barang);
                        $this->db->where("MONTH(tgl_transaksi) = ", $bulan);
                        $this->db->where("YEAR(tgl_transaksi) = ", $tahun);
                        $query = $this->db->get("transaksi");
                        return $query->num_rows();
                }

        }
?>